#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>

void color(int c, int b){
  HANDLE  hConsole;
  hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
  FlushConsoleInputBuffer(hConsole);
  SetConsoleTextAttribute(hConsole, c+16*b);
}

enum {NEGRO=0,AZUL,VERDE,CYAN,ROJO,MAGENTA,AMARILLO,BLANCO,BNEGRO=8,AZULB,VERDEB,CYANB,ROJOB,MAGENTAB,AMARILLOB,BLANCOB};

void xpersonaje(int x,int y,char z){
    //pies
    gotoxy(x,y);
    printf("%c%c%c%c%c",201,205,202,205,187);
    //brazos
    gotoxy(x,y-2);
    printf("%c%c%c%c%c",201,205,206,205,187);
    //torzo
    gotoxy(x+2,y-1);
    printf("%c",186);
    //cabeza
    gotoxy(x,y-5);
    printf(" %c%c%c",201,205,187);
    gotoxy(x,y-4);
    printf(" %c%c%c",186,z,186);
    gotoxy(x,y-3);
    printf(" %c%c%c",200,203,188);
}

void simbopersonaje(int x,int y,char z){

    gotoxy(x,y-5);
    printf(" %c%c%c%c%c",201,205,205,205,187);
    gotoxy(x,y-4);
    printf(" %c %c %c",186,z,186);
    gotoxy(x,y-3);
    printf(" %c%c%c%c%c",200,205,205,205,188);

}

void equipoizq(char a, char b, char c){
    color(CYANB,NEGRO);
    xpersonaje(60,12,a);
    xpersonaje(63,19,b);
    xpersonaje(60,26,c);
    color(BLANCO,NEGRO);
}

void equipoder(char d, char e, char f){
    color(ROJOB,NEGRO);
    xpersonaje(103,12,d);
    xpersonaje(100,19,e);
    xpersonaje(103,26,f);
    color(BLANCO,NEGRO);
}

void gotoxy(int x, int y){
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void puntoxy(int x, int y, char c){
gotoxy(x,y); printf("%c",c);
}

void printLine(int x1, int y1, int x2, int y2, char c){
    float dx, dy, adx, ady;
    dx=x2-x1; adx=dx<0?-1*dx:dx;
    dy=y2-y1; ady=dy<0?-1*dy:dy;
    float mayor = (adx>ady?adx:ady);
    for(int i=0;i<=mayor;i++){
            puntoxy(x1+i*(dx/mayor),y1+i*(dy/mayor),c);
    }
}

void recuadrolinea(int x1, int y1, int x2, int y2){
    printLine(x1,y1,x2,y1,196);
     printLine(x2,y1,x2,y2,179);
      printLine(x2,y2,x1,y2,196);
       printLine(x1,y2,x1,y1,179);

    puntoxy(x1,y1,218);
    puntoxy(x1,y2,192);
    puntoxy(x2,y1,191);
    puntoxy(x2,y2,217);
}

void recuadrodoble(int x1, int y1, int x2, int y2){
    printLine(x1,y1,x2,y1,205);
     printLine(x2,y1,x2,y2,186);
      printLine(x2,y2,x1,y2,205);
       printLine(x1,y2,x1,y1,186);

    puntoxy(x1,y1,201);
    puntoxy(x1,y2,200);
    puntoxy(x2,y1,187);
    puntoxy(x2,y2,188);
}

void recuadronegro(int x1, int y1, int x2, int y2){
    printLine(x1,y1,x2,y1,220);
     printLine(x2,y1,x2,y2,219);
      printLine(x2,y2,x1,y2,223);
       printLine(x1,y2,x1,y1,219);

    puntoxy(x1,y1,220);
    puntoxy(x1,y2,223);
    puntoxy(x2,y1,220);
    puntoxy(x2,y2,223);
}

void pestana(int x1,int y1,int x2,int y2){
    recuadronegro(x1,y1,x2,y2);
}

void cuadrocomandos(int x1,int y1,int x2,int y2){
    recuadrodoble(2,1,120,3);
}

void botonesinicio(char i){
    int x=38,y=25;
    recuadrodoble(x,y,x+40,y+8);
    recuadrodoble(x+48,y,x+88,y+8);
}

void pantallainicio(char i){
    int y=25;
    pestana(1,0,166,43);
    cuadrocomandos(2,1,72,3);
    botonesinicio('0');
    gotoxy(56,y+3);//opcion texto
    printf("JUGAR");
    gotoxy(56,y+5);//opcion letra
    printf("( j )");
    gotoxy(101,y+3);//opcion texto
    printf("�COMO JUGAR?");
    gotoxy(101,y+5);//opcion letra
    printf("   ( c )");
    cobelltitulo(177);
}

void recuadroseleccion(char i){
    system("cls");
    pestana(1,0,166,43);
    cuadrocomandos(2,1,72,3);

    int x1=3,y1=5,x2=55,y2=13;

    //recuadrodoble(x1,y1,x2,y2); //1
    //recuadrodoble(x1+54,y1,x2+54,y2); //2
    //recuadrodoble(x1+108,y1,x2+108,y2); //3

    recuadrodoble(x1,y1+9,x2,y2+9); //4
    recuadrodoble(x1+54,y1+9,x2+54,y2+9); //5
    recuadrodoble(x1+108,y1+9,x2+108,y2+9); //6

    recuadrodoble(x1,y1+18,x2,y2+18); //7
    recuadrodoble(x1+54,y1+18,x2+54,y2+18); //8
    recuadrodoble(x1+108,y1+18,x2+108,y2+18); //9

    //recuadrodoble(x1,y1+27,x2,y2+27); //10
    //recuadrodoble(x1+54,y1+27,x2+54,y2+27); //11
    //recuadrodoble(x1+108,y1+27,x2+108,y2+27); //12

}

void cobelltitulo(char i){
    //puntoxy(82,15,i);
    int x=85,y=11;

    printLine(x,y,x+10,y,i);      //E
    printLine(x,y+1,x+10,y+1,i);
    printLine(x,y+4,x+10,y+4,i);
    printLine(x,y+5,x+10,y+5,i);
    printLine(x,y+8,x+10,y+8,i);
    printLine(x,y+9,x+10,y+9,i);

    printLine(x+13,y,x+13,y+9,i);   //L 1
    printLine(x+14,y,x+14,y+9,i);
    printLine(x+15,y+8,x+23,y+8,i);
    printLine(x+15,y+9,x+23,y+9,i);

    printLine(x+26,y,x+26,y+9,i);   //L 2
    printLine(x+27,y,x+27,y+9,i);
    printLine(x+28,y+8,x+36,y+8,i);
    printLine(x+28,y+9,x+36,y+9,i);

    printLine(x-3,y,x-13,y,i);      //B
    printLine(x-3,y+1,x-13,y+1,i);
    printLine(x-3,y+4,x-13,y+4,i);
    printLine(x-3,y+5,x-13,y+5,i);
    printLine(x-3,y+8,x-13,y+8,i);
    printLine(x-3,y+9,x-13,y+9,i);
    printLine(x-3,y,x-3,y+9,i);
    printLine(x-4,y,x-4,y+9,i);
    printLine(x-13,y,x-13,y+9,i);
    printLine(x-12,y,x-12,y+9,i);
    puntoxy(x-3,y,' ');
    puntoxy(x-3,y+9,' ');


    printLine(x-16,y,x-16,y+9,i);   //O
    printLine(x-17,y,x-17,y+9,i);
    printLine(x-26,y,x-26,y+9,i);
    printLine(x-25,y,x-25,y+9,i);
    printLine(x-18,y,x-24,y,i);
    printLine(x-18,y+1,x-24,y+1,i);
    printLine(x-18,y+8,x-24,y+8,i);
    printLine(x-18,y+9,x-24,y+9,i);
    puntoxy(x-26,y,' ');
    puntoxy(x-26,y+9,' ');
    puntoxy(x-16,y+9,' ');
    puntoxy(x-16,y,' ');

    printLine(x-29,y,x-39,y,i);     //C
    printLine(x-29,y+1,x-39,y+1,i);
    printLine(x-29,y+8,x-39,y+8,i);
    printLine(x-29,y+9,x-39,y+9,i);
    printLine(x-39,y+2,x-39,y+7,i);
    printLine(x-38,y+2,x-38,y+7,i);

}

void enpartida(char i){
    system("cls");
    pestana(1,0,166,43);
    cuadrocomandos(2,1,72,3);

    color(CYANB,NEGRO);
    recuadrodoble(4,6,50,27); //derecha
    printLine(5,13,49,13,196);
    printLine(5,20,49,20,196);

    enpsim('0');

    FILE* p1ws = fopen("p1s.txt","r");
    int p1w = fgetc(p1ws);
    FILE* p2ws = fopen("p2s.txt","r");
    int p2w = fgetc(p2ws);
    FILE* p3ws = fopen("p3s.txt","r");
    int p3w = fgetc(p3ws);
    equipoizq(p1w,p2w,p3w);
    fclose(p1ws);
    fclose(p2ws);
    fclose(p3ws);
    color(CYANB,NEGRO);
    simbopersonaje(5,12,p1w);
    simbopersonaje(5,19,p2w);
    simbopersonaje(5,26,p3w);


    color(ROJOB,NEGRO);
    recuadrodoble(117,6,163,27);  //izquierda
    printLine(118,13,162,13,196);
    printLine(118,20,162,20,196);

    FILE* p4ws = fopen("p4s.txt","r");
    int p4w = fgetc(p4ws);
    FILE* p5ws = fopen("p5s.txt","r");
    int p5w = fgetc(p5ws);
    FILE* p6ws = fopen("p6s.txt","r");
    int p6w = fgetc(p6ws);
    equipoder(p4w,p5w,p6w);
    fclose(p4ws);
    fclose(p5ws);
    fclose(p6ws);
    color(ROJOB,NEGRO);
    simbopersonaje(118,12,p4w);
    simbopersonaje(118,19,p5w);
    simbopersonaje(118,26,p6w);

    color(BLANCOB,NEGRO);
    recuadrodoble(3,30,164,41);
    printLine(43,31,43,40,219);
    printLine(83,31,83,40,219);
    printLine(123,31,123,40,219);

    gotoxy(23,29);
    printf("1");
    gotoxy(60,29);
    printf("2");
    gotoxy(103,29);
    printf("3");
    gotoxy(143,29);
    printf("4");
}

void enpsim(char i){

color(BLANCOB,NEGRO);

    FILE* p1wsw = fopen("p1s.txt","w");   //archivo para poner simbolo    //kaisa
    FILE* p1ws = fopen("p1.txt","r");  //archivo para leer letra
    int p1w = fgetc(p1ws);
    if(p1w=='k'){
        gotoxy(8,11);
        printf("1");
        fputc(142,p1wsw);
    }
    fclose(p1wsw);
    fclose(p1ws);


    FILE* p2wsw = fopen("p2s.txt","w");   //archivo para poner simbolo    //morde
    FILE* p2ws = fopen("p2.txt","r");  //archivo para leer letra
    int p2w = fgetc(p2ws);
    if(p2w=='m'){
        gotoxy(8,18);
        printf("2");
        fputc(190,p2wsw);
    }
    fclose(p2wsw);
    fclose(p2ws);


    FILE* p3wsw = fopen("p3s.txt","w");   //archivo para poner simbolo    //sera
    FILE* p3ws = fopen("p3.txt","r");  //archivo para leer letra
    int p3w = fgetc(p3ws);
    if(p3w=='s'){
        gotoxy(8,25);
        printf("3");
        fputc(208,p3wsw);
    }
    fclose(p3wsw);
    fclose(p3ws);


    FILE* p4wsw = fopen("p4s.txt","w");   //archivo para poner simbolo    //heimer
    FILE* p4ws = fopen("p4.txt","r");  //archivo para leer letra
    int p4w = fgetc(p4ws);
    if(p4w=='h'){
        gotoxy(121,11);
        printf("1");
        fputc(159,p4wsw);
    }
    fclose(p4wsw);
    fclose(p4ws);


    FILE* p5wsw = fopen("p5s.txt","w");   //archivo para poner simbolo    //tanque
    FILE* p5ws = fopen("p5.txt","r");  //archivo para leer letra
    int p5w = fgetc(p5ws);
    if(p5w=='t'){
        gotoxy(121,18);
        printf("2");
        fputc(207,p5wsw);
    }
    fclose(p5wsw);
    fclose(p5ws);


    FILE* p6wsw = fopen("p6s.txt","w");   //archivo para poner simbolo    //drac
    FILE* p6ws = fopen("p6.txt","r");  //archivo para leer letra
    int p6w = fgetc(p6ws);
    if(p6w=='d'){
        gotoxy(121,25);
        printf("3");
        fputc(158,p6wsw);
    }
    fclose(p6wsw);
    fclose(p6ws);
}

void lineaturnod(char i){
    int x,y;
x=87;
y=13;
    color(BLANCOB,NEGRO);  //borra
    puntoxy(x,y,255);
    puntoxy(x-1,y+1,255);
    puntoxy(x-2,y+2,255);

    puntoxy(x-3,y+3,255);

    puntoxy(x-2,y+4,255);
    puntoxy(x-1,y+5,255);
    puntoxy(x,y+6,255);
    color(BLANCOB,NEGRO);
x=84;
y=13;
    color(NEGRO,BLANCOB);  //escribe
    puntoxy(x,y,92);
    puntoxy(x+1,y+1,92);
    puntoxy(x+2,y+2,92);

    puntoxy(x+3,y+3,255);

    puntoxy(x+2,y+4,47);
    puntoxy(x+1,y+5,47);
    puntoxy(x,y+6,47);
    color(BLANCOB,NEGRO);
}

void lineaturnoi(char i){
    int x,y;
x=84;
y=13;
    color(BLANCOB,NEGRO);  //borra
    puntoxy(x,y,255);
    puntoxy(x+1,y+1,255);
    puntoxy(x+2,y+2,255);

    puntoxy(x+3,y+3,255);

    puntoxy(x+2,y+4,255);
    puntoxy(x+1,y+5,255);
    puntoxy(x,y+6,255);
    color(BLANCOB,NEGRO);

x=87;
y=13;
    color(NEGRO,BLANCOB);  //escribe
    puntoxy(x,y,47);
    puntoxy(x-1,y+1,47);
    puntoxy(x-2,y+2,47);

    puntoxy(x-3,y+3,255);

    puntoxy(x-2,y+4,92);
    puntoxy(x-1,y+5,92);
    puntoxy(x,y+6,92);
    color(BLANCOB,NEGRO);
}

void siempresente(char i){
    pestana(1,0,166,43);
    cuadrocomandos(2,1,72,3);
}

void Ymenu(char i){
    int x=2;
    while(x!=1){
    x=2;
    char menu;
    FILE* menuelegido = fopen("menu.txt","w");
    system("cls");
    pantallainicio('0');
    gotoxy(4,2);
    printf("Escribe una letra (j � c) y presiona enter para continuar :  ");
    scanf("%c",&menu);
    switch(menu){
        case 'j':
            fputc('j',menuelegido);
            x=1;
            break;
        case 'c':
            fputc('c',menuelegido);
            x=1;
            break;
        default:
            x=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            break;
    }
    fclose(menuelegido);
    }
}

void quelegidomenu(char i){
    FILE* menuelegido = fopen("menu.txt","r");
    int eleccion = fgetc(menuelegido);
    if(eleccion=='j'){
        elegidojugar('0');
    }else{
        comojugar('0');
    }
    fclose(menuelegido);
    FILE* menuelegidoc = fopen("menu.txt","w"); //dejar en blanco menu.txt
    fputc(' ',menuelegidoc);
    fclose(menuelegidoc);

}

void botonescomojugar(char i){
    recuadrolinea(2,38,33,42); //pregugabilidad
    gotoxy(5,40);
    printf(" Prejugabilidad  -  ( p )");

    recuadrolinea(35,38,66,42); //roles
    gotoxy(40,40);
    printf("    Roles  -  ( r )");

    recuadrolinea(68,38,99,42); //champs
    gotoxy(70,40);
    printf("     Campeones  -  ( c )");

    recuadrolinea(101,38,132,42); //jugabilidad
    gotoxy(105,40);
    printf("  Jugabilidad  -  ( j )");

    recuadrolinea(134,38,165,42); //regresar
    gotoxy(140,40);
    printf("  Regresar  -  ( b )");
}

void cjjugabilidad(char i){
                                                                                                                     //hasta aqui
    gotoxy(71,6);
    color(NEGRO,BLANCOB);
    printf(" J U G A B I L I D A D ");
    color(BLANCOB,NEGRO);               //xp
    printf("\n\n\n \t\t\t\t");
    color(ROJOB,BLANCOB);
    printf(" XP ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\tCada campe�n cuenta con una cantidad de XP que ira aumentando con respecto a la cantidad de da�o infligido a los enemigos \n\t\t\tpor las habilidades del campe�n, y  o con la vida o armadura que se obtenga ya sea a partir de sus habilidades o por \n\t\t\thabilidades de tu equipo");

    color(BLANCOB,NEGRO);               //nivel de campeon
    printf("\n\n \t\t\t\t");
    color(ROJOB,BLANCOB);
    printf(" NIVEL DEL CAMPEON ");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\tLos campeones ascienden de nivel a partir de nivel 1 hasta el m�ximo nivel 4 a trav�s de la obtenci�n de XP, la cantidad de xp \n\t\t\tque requieren para cada nivel es la misma y no var�a entre campeones");

    color(BLANCOB,NEGRO);               //roles
    printf("\n\n \t\t\t\t");
    color(ROJOB,BLANCOB);
    printf(" ROLES ");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\tPara cada campe�n se tendr� que elegir un rol que le otorgar� estad�sticas adicionales ( pasiva ) a las del campe�n y que estar�n\n\t\t\tsiempre presentes adem�s de una habilidad �nica disponible al llegar que podr� ser usada cuando el campe�n alcance el nivel 4");

    color(BLANCOB,NEGRO);               //habilidades
    printf("\n\n \t\t\t\t");
    color(ROJOB,BLANCOB);
    printf(" HABILIDADES ");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\tCada campe�n posee 3 habilidades, que podr� desbloquear una a la vez por cada nivel que alcance (ya que el campe�n inicia nivel 1\n\t\t\tpodr� desbloquear una de sus habilidades como primera jugada)");

    color(BLANCOB,NEGRO);               //turnos
    printf("\n\n \t\t\t\t");
    color(ROJOB,BLANCOB);
    printf(" TURNOS ");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\tCOBELL es un juego de 1 vs 1 por lo que tiene que ser jugado con �nicamente 2 jugadores, los cuales inicialmente tendr�n que\n\t\t\telegir a su equipo de campeones");
    printf("\n \t\t\tDurante la partida, la participaci�n ser� por turnos en los cuales el invocador tendr� que elegir a uno de sus campeones,\n\t\t\tdesbloquear una de sus habilidades si el nivel del campe�n aumento, para despu�s elegir una de sus habilidades y si as�\n\t\t\tlo requiere, elegir al campe�n enemigo al que se le aplicar� la habilidad en cuesti�n");

    color(BLANCOB,NEGRO);
}

void cjprej(char i){
                                                                                                                         //hasta aqui
    gotoxy(68,6);
    color(NEGRO,BLANCOB);
    printf(" P R E - J U G A B I L I D A D ");

    color(BLANCOB,NEGRO);               //invocador
    printf("\n\n\n\n \t\t\t\t");
    color(VERDE,BLANCOB);
    printf(" INVOCADOR ");
    color(BLANCOB,NEGRO);                                                                                                                        //------                                                                                                                     //-----
    printf("\n\n\n \t\t\tEn el juego se te es llamado como invocador, debido a que invocas a los campeones que eliges");

    color(BLANCOB,NEGRO);               //seleccion de campeones
    printf("\n\n\n \t\t\t\t");
    color(VERDE,BLANCOB);
    printf(" SELLECION DE CAMPEONES ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\tCada invocador tendr� que elegir  a 3 campeones de los 12 disponibles sin repetir alguno, los cuales conformar�n a su equipo");

    color(BLANCOB,NEGRO);               //seleccion de roles
    printf("\n\n\n \t\t\t\t");
    color(VERDE,BLANCOB);
    printf(" SELECION DE ROLES ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\tCada invocador tendr� que elegir un rol para cada uno de los campeones que haya previamente elegido; solo podr� elegir los\n\t\t\troles una vez por lo que en el equipo de campeones nunca se repetir� alg�n rol");

    color(BLANCOB,NEGRO);               //victoria
    printf("\n\n\n \t\t\t\t");
    color(VERDE,BLANCOB);
    printf(" VICTORIA ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\tLa victoria se le otorgara en el momento en que uno de los invocadores se quede sin campeones con vida, evidentemente ganar�\n\t\t\taquel que aun tenga al menos a un campe�n con vida");

    color(BLANCOB,NEGRO);
}

void cjroles(char i){

    gotoxy(78,5);
    color(NEGRO,BLANCOB);                                                                                                               //
    printf(" R O L E S ");

    color(BLANCOB,NEGRO);               //vampiro
    printf("\n\n \t\t\t\t");
    color(ROJO,CYAN);
    printf(" VAMPIRO ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\t");
    color(MAGENTAB,NEGRO);
    printf(" Pasiva: ");
    color(BLANCOB,NEGRO);
    printf("El % del da�o total que realices se convierte en vida que adquieres");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\t");
    color(ROJOB,NEGRO);
    printf(" Habilidad: ");
    color(BLANCOB,NEGRO);
    printf("Elige un campe�n aliado y aumentar la vida que ha adquirido");

    color(BLANCOB,NEGRO);               //taxi
    printf("\n\n \t\t\t\t");
    color(BLANCO,CYAN);
    printf(" TAXIDERMISTA ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\t");
    color(MAGENTAB,NEGRO);
    printf(" Pasiva: ");
    color(BLANCOB,NEGRO);
    printf("El % del da�o total que realices se convierte en armadura que adquieres");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\t");
    color(ROJOB,NEGRO);
    printf(" Habilidad: ");
    color(BLANCOB,NEGRO);
    printf("Elige un campe�n aliado y aumentar la armadura que tienes");

    color(BLANCOB,NEGRO);               //ptrd
    printf("\n\n \t\t\t\t");
    color(CYANB,CYAN);
    printf(" PTRD ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\t");
    color(MAGENTAB,NEGRO);
    printf(" Pasiva: ");
    color(BLANCOB,NEGRO);
    printf("El da�o producido se ve afectado solo por el % de armadura y el resto inflige da�o a la vida");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\t");
    color(ROJOB,NEGRO);
    printf(" Habilidad: ");
    color(BLANCOB,NEGRO);
    printf("El da�o producido ignora por completo la armadura del enemigo y se aplica a su vida");

    color(BLANCOB,NEGRO);               //guardian
    printf("\n\n \t\t\t\t");
    color(VERDEB,CYAN);
    printf(" GUARDIAN ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\t");
    color(MAGENTAB,NEGRO);
    printf(" Pasiva: ");
    color(BLANCOB,NEGRO);
    printf("Inflige x de curaci�n a aliados y a s� mismo por cada turno");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\t");
    color(ROJOB,NEGRO);
    printf(" Habilidad: ");
    color(BLANCOB,NEGRO);
    printf("Vuelve inmune a todo equipo para el siguiente ataque enemigo");

    color(BLANCOB,NEGRO);               //genio
    printf("\n\n \t\t\t\t");
    color(AMARILLOB,CYAN);
    printf(" GENIO ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\t");
    color(MAGENTAB,NEGRO);
    printf(" Pasiva: ");
    color(BLANCOB,NEGRO);
    printf("El xp obtenido se ve multiplicado por 2");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\t");
    color(ROJOB,NEGRO);
    printf(" Habilidad: ");
    color(BLANCOB,NEGRO);
    printf("Aumenta en un nivel a un aliado");

    color(BLANCOB,NEGRO);               //asesino
    printf("\n\n \t\t\t\t");
    color(NEGRO,CYAN);
    printf(" ASESINO ");
    color(BLANCOB,NEGRO);
    printf("\n\n \t\t\t");
    color(MAGENTAB,NEGRO);
    printf(" Pasiva: ");
    color(BLANCOB,NEGRO);
    printf("Aumenta en % el da�o del campe�n");
    color(BLANCOB,NEGRO);
    printf("\n \t\t\t");
    color(ROJOB,NEGRO);
    printf(" Habilidad: ");
    color(BLANCOB,NEGRO);
    printf("Aumenta en % el da�o del campe�n y se permanece hasta que se use otra habilidad del mismo campe�n en otro turno");


    color(BLANCOB,NEGRO);
}

void cjchamps(char i){
                                                                                                                         //hasta aqui
    gotoxy(74,6);
    color(NEGRO,BLANCOB);                   //
    printf(" C A M P E O N E S ");

    color(BLANCOB,NEGRO);               //kaisa
    printf("\n\n \t\t\t\t    ");
    color(VERDEB,ROJO);
    printf(" KAISA ");
    color(BLANCOB,NEGRO);                                                                                                                        //------                                                                                                                     //-----
    printf(" \n \t\t\t\t ");
    color(MAGENTA,NEGRO);
    printf("190 de vida");
    color(BLANCOB,NEGRO);                                                //
    printf("  \n \t\t\t\t");
    color(MAGENTA,NEGRO);
    printf("0 de Armadura");
    color(BLANCOB,NEGRO);                                              //
    printf("  \n \t 1. Lanza un ataque a un campe�n enemigo que inflige 20 de da�o");
    printf("  \n \t 2. Lanza un ataque contra todos lo enemigos que infligen 10 de \n\t    da�o");
    printf("  \n \t 3. Lanza un ataque contra un enemigo que disminuye 5 de vida y \n\t    inflige 30 de da�o");

    color(BLANCOB,NEGRO);               //morde
    printf("\n\n\n \t\t\t\t");
    color(VERDEB,ROJO);
    printf(" MORDEKAISER ");
    color(BLANCOB,NEGRO);                                                                                                                        //------                                                                                                                     //-----
    printf(" \n \t\t\t\t ");
    color(MAGENTA,NEGRO);
    printf("200 de vida");
    color(BLANCOB,NEGRO);                                                //
    printf("  \n \t\t\t\t");
    color(MAGENTA,NEGRO);
    printf("20 de Armadura");
    color(BLANCOB,NEGRO);                                                          //
    printf("  \n \t 1. Se cura a si mismo 6 de vida");
    printf("  \n \t 2. Lanza un ataque a un campe�n enemigo que inflige 20 de da�o");
    printf("  \n \t 3. Se aumenta a si mismo 3 de armadura");

    color(BLANCOB,NEGRO);               //serafine
    printf("\n\n\n \t\t\t\t  ");
    color(VERDEB,ROJO);
    printf(" SERAFINE ");
    color(BLANCOB,NEGRO);                                                                                                                        //------                                                                                                                     //-----
    printf(" \n \t\t\t\t ");
    color(MAGENTA,NEGRO);
    printf("220 de vida");
    color(BLANCOB,NEGRO);                                                //
    printf("  \n \t\t\t\t");
    color(MAGENTA,NEGRO);
    printf("0 de Armadura");
    color(BLANCOB,NEGRO);                                              //
    printf("  \n \t 1. Da 15 de vida a un aliado");
    printf("  \n \t 2. Lanza un ataque a un campe�n enemigo que inflige 10 de da�o");
    printf("  \n \t 3. Se cura a s� misma 30 de vida");

    gotoxy(120,8);                         //heimer
    color(VERDEB,ROJO);
    printf(" HEIMERDINGER ");
    gotoxy(121,9);
    color(MAGENTA,NEGRO);
    printf("200 de vida");
    gotoxy(120,10);
    color(MAGENTA,NEGRO);
    printf("10 de Armadura");
    color(BLANCOB,NEGRO);
    gotoxy(95,11);                                            //
    printf("1. Lanza un ataque a todos los enemigos que inflige 10  de da�o");                           /////
    gotoxy(95,12);
    printf("2. Se aumenta a si mismo 3 de armadura");
    gotoxy(95,13);
    printf("3. Lanza un ataque a un enemigo que inflige 30 de da�o pero le");
    gotoxy(95,14);
    printf("   disminuye 5 de armadura");

    gotoxy(122,18);                         //tankech
    color(VERDEB,ROJO);
    printf(" TANKECH ");
    gotoxy(121,19);
    color(MAGENTA,NEGRO);
    printf("200 de vida");
    gotoxy(120,20);
    color(MAGENTA,NEGRO);
    printf("30 de Armadura");
    color(BLANCOB,NEGRO);
    gotoxy(95,21);                                            //
    printf("1. Se aumenta a si mismo 6 de aramdura");
    gotoxy(95,22);
    printf("2. Aumenta a un aliado 4 de armadura");
    gotoxy(95,23);
    printf("3. Lanza un ataque a un enemigo que inflige 10 de da�o");

    gotoxy(123,26);                         //draco
    color(VERDEB,ROJO);
    printf(" DRACO ");
    gotoxy(121,27);
    color(MAGENTA,NEGRO);
    printf("210 de vida");
    gotoxy(120,28);
    color(MAGENTA,NEGRO);
    printf("0 de Armadura");
    color(BLANCOB,NEGRO);
    gotoxy(95,29);                                            //
    printf("1. Se cura a si mismo 10 de vida");
    gotoxy(95,30);
    printf("2. Lanza un a ataque a todos los enemigos que inflige 5 de da�o");
    gotoxy(95,31);
    printf("3. Lanza un ataque que inflige 15 de da�o");

    color(BLANCOB,NEGRO);
}

void comojugar(char i){
   system("cls");
   siempresente('0');
   botonescomojugar('0');
   cjprej('0');

   char opcioncj;
   int x=2;
   while(x=2){

   //x=1;
   opcioncj='p';
   gotoxy(4,2);
   printf("                                                                                                                  ");
   gotoxy(4,2);
   printf("Escribe una letra de la opcion que decees y presiona enter : ");
   scanf("%c",&opcioncj);

   switch(opcioncj){
        case 'j':
            system("cls");
            siempresente('0');
            botonescomojugar('0');
            cjjugabilidad('0');       //elije jugabilidad
            x=2;
            break;
        case 'r':
            system("cls");
            siempresente('0');
            botonescomojugar('0');
            cjroles('0');             //elije roles
            x=2;
            break;
        case 'p':
            system("cls");
            siempresente('0');
            botonescomojugar('0');
            cjprej('0');       //elije pre-jugabi...
            x=2;
            break;
        case 'c':
            system("cls");
            siempresente('0');
            botonescomojugar('0');
            cjchamps('0');            //elije champs
            x=2;
            break;

        case 'b':
            x=1;                         //elije backear

            FILE* menur5 = fopen("menuretorno.txt","w"); //dejar en blanco menu.txt
            fputc('r',menur5);
            fclose(menur5);
            break;

        default:
            x=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");

    }
if(x!=2){break;}
    }
}

void elegidojugar(char i){
    recuadroseleccion('0');
    wopcionesdechamps('0');
    selechamps('0');

    recuadroseleccion('0');
    wopcionesderol('0');
    seleroles('0');

    enpartida('0');
    gotoxy(4,2);
    printf("                                                                                                                 ");
    color(BLANCOB,NEGRO);
    gotoxy(4,2);
    printf("LA PARTIDA COMIENZA, El azul representa al equipo 1 y el rojo al 2. ");
    system("pause");

    empieza('0');

}

void empieza(char i){

    int p1,p2,p3,p4,p5,p6,r1,r2,r3,r4,r5,r6;

    FILE* punoa = fopen("p1.txt","r");
    FILE* pdosa = fopen("p2.txt","r");
    FILE* ptrea = fopen("p3.txt","r");
    FILE* pcuaa = fopen("p4.txt","r");
    FILE* pcina = fopen("p5.txt","r");
    FILE* pseia = fopen("p6.txt","r");

    int p1a = fgetc(punoa);
    int p2a = fgetc(pdosa);
    int p3a = fgetc(ptrea);
    int p4a = fgetc(pcuaa);
    int p5a = fgetc(pcina);
    int p6a = fgetc(pseia);

    FILE* runoa = fopen("r1.txt","r");
    FILE* rdosa = fopen("r2.txt","r");
    FILE* rtrea = fopen("r3.txt","r");
    FILE* rcuaa = fopen("r4.txt","r");
    FILE* rcina = fopen("r5.txt","r");
    FILE* rseia = fopen("r6.txt","r");

    int r1a = fgetc(runoa);
    int r2a = fgetc(rdosa);
    int r3a = fgetc(rtrea);
    int r4a = fgetc(rcuaa);
    int r5a = fgetc(rcina);
    int r6a = fgetc(rseia);

    if(p1a=='k'){p1=1;}
    if(p1a=='m'){p1=2;}
    if(p1a=='s'){p1=3;}
    if(p1a=='h'){p1=4;}
    if(p1a=='t'){p1=5;}
    if(p1a=='d'){p1=6;}

    if(p2a=='k'){p2=1;}
    if(p2a=='m'){p2=2;}
    if(p2a=='s'){p2=3;}
    if(p2a=='h'){p2=4;}
    if(p2a=='t'){p2=5;}
    if(p2a=='d'){p2=6;}

    if(p3a=='k'){p3=1;}
    if(p3a=='m'){p3=2;}
    if(p3a=='s'){p3=3;}
    if(p3a=='h'){p3=4;}
    if(p3a=='t'){p3=5;}
    if(p3a=='d'){p3=6;}

    if(p4a=='k'){p4=1;}
    if(p4a=='m'){p4=2;}
    if(p4a=='s'){p4=3;}
    if(p4a=='h'){p4=4;}
    if(p4a=='t'){p4=5;}
    if(p4a=='d'){p4=6;}

    if(p5a=='k'){p5=1;}
    if(p5a=='m'){p5=2;}
    if(p5a=='s'){p5=3;}
    if(p5a=='h'){p5=4;}
    if(p5a=='t'){p5=5;}
    if(p5a=='d'){p5=6;}

    if(p6a=='k'){p6=1;}
    if(p6a=='m'){p6=2;}
    if(p6a=='s'){p6=3;}
    if(p6a=='h'){p6=4;}
    if(p6a=='t'){p6=5;}
    if(p6a=='d'){p6=6;}

    if(r1a=='v'){r1=1;}
    if(r1a=='t'){r1=2;}
    if(r1a=='p'){r1=3;}
    if(r1a=='g'){r1=4;}
    if(r1a=='e'){r1=5;}
    if(r1a=='a'){r1=6;}

    if(r2a=='v'){r2=1;}
    if(r2a=='t'){r2=2;}
    if(r2a=='p'){r2=3;}
    if(r2a=='g'){r2=4;}
    if(r2a=='e'){r2=5;}
    if(r2a=='a'){r2=6;}

    if(r3a=='v'){r3=1;}
    if(r3a=='t'){r3=2;}
    if(r3a=='p'){r3=3;}
    if(r3a=='g'){r3=4;}
    if(r3a=='e'){r3=5;}
    if(r3a=='a'){r3=6;}

    if(r4a=='v'){r4=1;}
    if(r4a=='t'){r4=2;}
    if(r4a=='p'){r4=3;}
    if(r4a=='g'){r4=4;}
    if(r4a=='e'){r4=5;}
    if(r4a=='a'){r4=6;}

    if(r5a=='v'){r5=1;}
    if(r5a=='t'){r5=2;}
    if(r5a=='p'){r5=3;}
    if(r5a=='g'){r5=4;}
    if(r5a=='e'){r5=5;}
    if(r5a=='a'){r5=6;}

    if(r6a=='v'){r6=1;}
    if(r6a=='t'){r6=2;}
    if(r6a=='p'){r6=3;}
    if(r6a=='g'){r6=4;}
    if(r6a=='e'){r6=5;}
    if(r6a=='a'){r6=6;}

    fclose(punoa);
    fclose(pdosa);
    fclose(ptrea);
    fclose(pcuaa);
    fclose(pcina);
    fclose(pseia);

    fclose(runoa);
    fclose(rdosa);
    fclose(rtrea);
    fclose(rcuaa);
    fclose(rcina);
    fclose(rseia);

    yaconvariables(p1,p2,p3,p4,p5,p6,r1,r2,r3,r4,r5,r6);

}

void yaconvariables(int p1,int p2,int p3,int p4,int p5,int p6,int r1, int r2,int r3,int r4,int r5,int r6){

int ya;
    int v1,a1;
    int dh11,dh12,dh13;
    int vh11,vh12,vh13;
    int ah11,ah12,ah13;
    int ph11,ph12,ph13;
    if(p1=1){          //k
        v1=190;
        a1=0;

        ph11=2;
        ph12=3;
        ph13=2;

        dh11=20;
        dh12=10;
        dh13=0;

        vh11=0;
        vh12=0;
        vh13=-5;

        ah11=0;
        ah12=0;
        ah13=0;}
    if(p1=2){          //m
        v1=200;
        a1=20;

        ph11=1;
        ph12=2;
        ph13=1;

        dh11=0;
        dh12=20;
        dh13=0;

        vh11=6;
        vh12=0;
        vh13=0;

        ah11=0;
        ah12=0;
        ah13=3;}
    if(p1=3){          //s
        v1=220;
        a1=0;

        ph11=4;
        ph12=2;
        ph13=1;

        dh11=-10;
        dh12=10;
        dh13=0;

        vh11=0;
        vh12=0;
        vh13=30;

        ah11=0;
        ah12=0;
        ah13=0;}
    if(p1=4){          //h
        v1=200;
        a1=10;

        ph11=3;
        ph12=4;
        ph13=2;

        dh11=10;
        dh12=-3;
        dh13=30;

        vh11=0;
        vh12=0;
        vh13=0;

        ah11=0;
        ah12=0;
        ah13=-5;}
    if(p1=5){          //t
        v1=200;
        a1=30;

        ph11=1;
        ph12=4;
        ph13=2;

        dh11=0;
        dh12=-4;
        dh13=10;

        vh11=0;
        vh12=0;
        vh13=0;

        ah11=6;
        ah12=0;
        ah13=0;}
    if(p1=6){          //d
        v1=210;
        a1=0;

        ph11=1;
        ph12=3;
        ph13=2;

        dh11=0;
        dh12=5;
        dh13=15;

        vh11=10;
        vh12=0;
        vh13=0;

        ah11=0;
        ah12=0;
        ah13=0;}

    int v2,a2;
    int dh21,dh22,dh23;
    int vh21,vh22,vh23;
    int ah21,ah22,ah23;
    int ph21,ph22,ph23;
    if(p2=1){          //k
        v2=190;
        a2=0;

        ph21=2;
        ph22=3;
        ph23=2;

        dh21=20;
        dh22=10;
        dh23=0;

        vh21=0;
        vh22=0;
        vh23=-5;

        ah21=0;
        ah22=0;
        ah23=0;}
    if(p2=2){          //m
        v2=200;
        a2=20;

        ph21=1;
        ph22=2;
        ph23=1;

        dh21=0;
        dh22=20;
        dh23=0;

        vh21=6;
        vh22=0;
        vh23=0;

        ah21=0;
        ah22=0;
        ah23=3;}
    if(p2=3){          //s
        v2=220;
        a2=0;

        ph21=4;
        ph22=2;
        ph23=1;

        dh21=-10;
        dh22=10;
        dh23=0;

        vh21=0;
        vh22=0;
        vh23=30;

        ah21=0;
        ah22=0;
        ah23=0;}
    if(p2=4){          //h
        v2=200;
        a2=10;

        ph21=3;
        ph22=4;
        ph23=2;

        dh21=10;
        dh22=-3;
        dh23=30;

        vh21=0;
        vh22=0;
        vh23=0;

        ah21=0;
        ah22=0;
        ah23=-5;}
    if(p2=5){          //t
        v2=200;
        a2=30;

        ph21=1;
        ph22=4;
        ph23=2;

        dh21=0;
        dh22=-4;
        dh23=10;

        vh21=0;
        vh22=0;
        vh23=0;

        ah21=6;
        ah22=0;
        ah23=0;}
    if(p2=6){          //d
        v2=210;
        a2=0;

        ph21=1;
        ph22=3;
        ph23=2;

        dh21=0;
        dh22=5;
        dh23=15;

        vh21=10;
        vh22=0;
        vh23=0;

        ah21=0;
        ah22=0;
        ah23=0;}

    int v3,a3;
    int dh31,dh32,dh33;
    int vh31,vh32,vh33;
    int ah31,ah32,ah33;
    int ph31,ph32,ph33;
    if(p3=1){          //k
        v3=190;
        a3=0;

        ph31=2;
        ph32=3;
        ph33=2;

        dh31=20;
        dh32=10;
        dh33=0;

        vh31=0;
        vh32=0;
        vh33=-5;

        ah31=0;
        ah32=0;
        ah33=0;}
    if(p3=2){          //m
        v3=200;
        a3=20;

        ph31=1;
        ph32=2;
        ph33=1;

        dh31=0;
        dh32=20;
        dh33=0;

        vh31=6;
        vh32=0;
        vh33=0;

        ah31=0;
        ah32=0;
        ah33=3;}
    if(p3=3){          //s
        v3=220;
        a3=0;

        ph31=4;
        ph32=2;
        ph33=1;

        dh31=-10;
        dh32=10;
        dh33=0;

        vh31=0;
        vh32=0;
        vh33=30;

        ah31=0;
        ah32=0;
        ah33=0;}
    if(p3=4){          //h
        v3=200;
        a3=10;

        ph31=3;
        ph32=4;
        ph33=2;

        dh31=10;
        dh32=-3;
        dh33=30;

        vh31=0;
        vh32=0;
        vh33=0;

        ah31=0;
        ah32=0;
        ah33=-5;}
    if(p3=5){          //t
        v3=200;
        a3=30;

        ph31=1;
        ph32=4;
        ph33=2;

        dh31=0;
        dh32=-4;
        dh33=10;

        vh31=0;
        vh32=0;
        vh33=0;

        ah31=6;
        ah32=0;
        ah33=0;}
    if(p3=6){          //d
        v3=210;
        a3=0;

        ph31=1;
        ph32=3;
        ph33=2;

        dh31=0;
        dh32=5;
        dh33=15;

        vh31=10;
        vh32=0;
        vh33=0;

        ah31=0;
        ah32=0;
        ah33=0;}

    int v4,a4;
    int dh41,dh42,dh43;
    int vh41,vh42,vh43;
    int ah41,ah42,ah43;
    int ph41,ph42,ph43;
    if(p4=1){          //k
        v4=190;
        a4=0;

        ph41=2;
        ph42=3;
        ph43=2;

        dh41=20;
        dh42=10;
        dh43=0;

        vh41=0;
        vh42=0;
        vh43=-5;

        ah41=0;
        ah42=0;
        ah43=0;}
    if(p4=2){          //m
        v4=200;
        a4=20;

        ph41=1;
        ph42=2;
        ph43=1;

        dh41=0;
        dh42=20;
        dh43=0;

        vh41=6;
        vh42=0;
        vh43=0;

        ah41=0;
        ah42=0;
        ah43=3;}
    if(p4=3){          //s
        v4=220;
        a4=0;

        ph41=4;
        ph42=2;
        ph43=1;

        dh41=-10;
        dh42=10;
        dh43=0;

        vh41=0;
        vh42=0;
        vh43=30;

        ah41=0;
        ah42=0;
        ah43=0;}
    if(p4=4){          //h
        v4=200;
        a4=10;

        ph41=3;
        ph42=4;
        ph43=2;

        dh41=10;
        dh42=-3;
        dh43=30;

        vh41=0;
        vh42=0;
        vh43=0;

        ah41=0;
        ah42=0;
        ah43=-5;}
    if(p4=5){          //t
        v4=200;
        a4=30;

        ph41=1;
        ph42=4;
        ph43=2;

        dh41=0;
        dh42=-4;
        dh43=10;

        vh41=0;
        vh42=0;
        vh43=0;

        ah41=6;
        ah42=0;
        ah43=0;}
    if(p4=6){          //d
        v4=210;
        a4=0;

        ph41=1;
        ph42=3;
        ph43=2;

        dh41=0;
        dh42=5;
        dh43=15;

        vh41=10;
        vh42=0;
        vh43=0;

        ah41=0;
        ah42=0;
        ah43=0;}

    int v5,a5;
    int dh51,dh52,dh53;
    int vh51,vh52,vh53;
    int ah51,ah52,ah53;
    int ph51,ph52,ph53;
    if(p5=1){          //k
        v5=190;
        a5=0;

        ph51=2;
        ph52=3;
        ph53=2;

        dh51=20;
        dh52=10;
        dh53=0;

        vh51=0;
        vh52=0;
        vh53=-5;

        ah51=0;
        ah52=0;
        ah53=0;}
    if(p5=2){          //m
        v5=200;
        a5=20;

        ph51=1;
        ph52=2;
        ph53=1;

        dh51=0;
        dh52=20;
        dh53=0;

        vh51=6;
        vh52=0;
        vh53=0;

        ah51=0;
        ah52=0;
        ah53=3;}
    if(p5=3){          //s
        v5=220;
        a5=0;

        ph51=4;
        ph52=2;
        ph53=1;

        dh51=-10;
        dh52=10;
        dh53=0;

        vh51=0;
        vh52=0;
        vh53=30;

        ah51=0;
        ah52=0;
        ah53=0;}
    if(p5=4){          //h
        v5=200;
        a5=10;

        ph51=3;
        ph52=4;
        ph53=2;

        dh51=10;
        dh52=-3;
        dh53=30;

        vh51=0;
        vh52=0;
        vh53=0;

        ah51=0;
        ah52=0;
        ah53=-5;}
    if(p5=5){          //t
        v5=200;
        a5=30;

        ph51=1;
        ph52=4;
        ph53=2;

        dh51=0;
        dh52=-4;
        dh53=10;

        vh51=0;
        vh52=0;
        vh53=0;

        ah51=6;
        ah52=0;
        ah53=0;}
    if(p5=6){          //d
        v5=210;
        a5=0;

        ph51=1;
        ph52=3;
        ph53=2;

        dh51=0;
        dh52=5;
        dh53=15;

        vh51=10;
        vh52=0;
        vh53=0;

        ah51=0;
        ah52=0;
        ah53=0;}

    int v6,a6;
    int dh61,dh62,dh63;
    int vh61,vh62,vh63;
    int ah61,ah62,ah63;
    int ph61,ph62,ph63;
    if(p6=1){          //k
        v6=190;
        a6=0;

        ph61=2;
        ph62=3;
        ph63=2;

        dh61=20;
        dh62=10;
        dh63=0;

        vh61=0;
        vh62=0;
        vh63=-5;

        ah61=0;
        ah62=0;
        ah63=0;}
    if(p6=2){          //m
        v6=200;
        a6=20;

        ph61=1;
        ph62=2;
        ph63=1;

        dh61=0;
        dh62=20;
        dh63=0;

        vh61=6;
        vh62=0;
        vh63=0;

        ah61=0;
        ah62=0;
        ah63=3;}
    if(p6=3){          //s
        v6=220;
        a6=0;

        ph61=4;
        ph62=2;
        ph63=1;

        dh61=-10;
        dh62=10;
        dh63=0;

        vh61=0;
        vh62=0;
        vh63=30;

        ah61=0;
        ah62=0;
        ah63=0;}
    if(p6=4){          //h
        v6=200;
        a6=10;

        ph61=3;
        ph62=4;
        ph63=2;

        dh61=10;
        dh62=-3;
        dh63=30;

        vh61=0;
        vh62=0;
        vh63=0;

        ah61=0;
        ah62=0;
        ah63=-5;}
    if(p6=5){          //t
        v6=200;
        a6=30;

        ph61=1;
        ph62=4;
        ph63=2;

        dh61=0;
        dh62=-4;
        dh63=10;

        vh61=0;
        vh62=0;
        vh63=0;

        ah61=6;
        ah62=0;
        ah63=0;}
    if(p6=6){          //d
        v6=210;
        a6=0;

        ph61=1;
        ph62=3;
        ph63=2;

        dh61=0;
        dh62=5;
        dh63=15;

        vh61=10;
        vh62=0;
        vh63=0;

        ah61=0;
        ah62=0;
        ah63=0;}

    color(VERDEB,NEGRO);
    gotoxy(12,9);
    printf("VIDA = %i",v1);
    gotoxy(12,16);
    printf("VIDA = %i",v1);
    gotoxy(12,23);
    printf("VIDA = %i",v1);

    gotoxy(125,9);
    printf("VIDA = %i",v1);
    gotoxy(125,16);
    printf("VIDA = %i",v1);
    gotoxy(125,23);
    printf("VIDA = %i",v1);

    int nc,nh,nhm,nce;
    int lvl1=0,lvl2=0,lvl3=0,lvl4=0,lvl5=0,lvl6=0;
    int la1=0,la2=0,la3=0,la4=0,la5=0,la6=0;
    //int ilvl1=0,ilvl2=0,ilvl3=0,ilvl4=0,ilvl5=0,ilvl6=0;
    int hh11=0,hh12=0,hh13=0,hh21=0,hh22=0,hh23=0,hh31=0,hh32=0,hh33=0,hh41=0,hh42=0,hh43=0,hh51=0,hh52=0,hh53=0,hh61=0,hh62=0,hh63=0;
    int xp1=61,xp2=61,xp3=61,xp4=61,xp5=61,xp6=61;

    do{
        lineaturnod('0');
        gotoxy(4,2);
        printf("                                                                                                                ");
        color(CYANB,NEGRO);
        gotoxy(4,2);
        printf("Escriba el numero del campeon con el que desea jugar el turno: ");
        scanf("%i",&nc);

        switch(nc){

            case 1:
                whanilidadesgeneral(p1);
                if(la1<4){
                if(xp1>59){
                lvl1=lvl1+1;
                la1=la1+1;
                xp1=0;}
                if(lvl1=1){
                lvl1=0;
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escribe el numero de la habilidad que desees habilitar y presiona enter : ");
                scanf("%i",&nhm);
                switch(nhm){
                case 1:
                    hh11=1;
                    break;
                case 2:
                    hh12=1;
                    break;
                case 3:
                    hh13=1;
                    break;
                default:
                    gotoxy(4,2);
                    printf("Puede que el numero que escogiste no sea una opcion, sea o no es el caso, ");
                    system("pause");
                }
                }}
                aqui3:
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escriba el numero de habilidad que desea usar: ");
                scanf("%i",&nh);
                switch(nh){
                 case 1:
                    if(hh11=1){
                        switch(ph11){
                            case 1:
                                v1=v1-vh11;
                                a1=a1-ah11;
                                break;
                            case 2:
                                aqui1:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh11-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh11-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh11-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui1;
                                }
                                break;
                            case 3:
                                v4=v4-(dh11-a4);
                                v5=v5-(dh11-a5);
                                v6=v6-(dh11-a6);
                                break;
                            case 4:
                                aqui2:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh11;
                                        a1=a1-ah11;
                                        break;
                                    case 2:
                                        v2=v2-vh11;
                                        a2=a2-ah11;
                                        break;
                                    case 3:
                                        v3=v3-vh11;
                                        a3=a3-ah11;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui2;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui3;
                    }
                 break;
                 case 2:
                    if(hh12=1){
                        switch(ph12){
                            case 1:
                                v1=v1-vh12;
                                a1=a1-ah12;
                                break;
                            case 2:
                                aqui21:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh12-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh12-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh12-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui21;
                                }
                                break;
                            case 3:
                                v4=v4-(dh12-a4);
                                v5=v5-(dh12-a5);
                                v6=v6-(dh12-a6);
                                break;
                            case 4:
                                aqui22:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh12;
                                        a1=a1-ah12;
                                        break;
                                    case 2:
                                        v2=v2-vh12;
                                        a2=a2-ah12;
                                        break;
                                    case 3:
                                        v3=v3-vh12;
                                        a3=a3-ah12;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui22;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui3;
                    }
                 break;
                 case 3:
                    if(hh13=1){
                        switch(ph13){
                            case 1:
                                v1=v1-vh13;
                                a1=a1-ah13;
                                break;
                            case 2:
                                aqui31:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh13-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh13-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh13-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui31;
                                }
                                break;
                            case 3:
                                v4=v4-(dh13-a4);
                                v5=v5-(dh13-a5);
                                v6=v6-(dh13-a6);
                                break;
                            case 4:
                                aqui32:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh13;
                                        a1=a1-ah13;
                                        break;
                                    case 2:
                                        v2=v2-vh13;
                                        a2=a2-ah13;
                                        break;
                                    case 3:
                                        v3=v3-vh13;
                                        a3=a3-ah13;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui32;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui3;
                    }
                 break;
                 default:
                 printf("El nuemro que elegiste no es una opcion");
                 system("pause");
                 goto aqui3;
                }
            break;


            case 2:
                whanilidadesgeneral(p2);
                if(la2<4){
                if(xp2>59){
                lvl2=lvl2+1;
                la2=la2+1;
                xp2=0;}
                if(lvl2=1){
                lvl2=0;
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escribe el numero de la habilidad que desees habilitar y presiona enter : ");
                scanf("%i",&nhm);
                switch(nhm){
                case 1:
                    hh21=1;
                    break;
                case 2:
                    hh22=1;
                    break;
                case 3:
                    hh23=1;
                    break;
                default:
                    gotoxy(4,2);
                    printf("Puede que el numero que escogiste no sea una opcion, sea o no es el caso, ");
                    system("pause");
                }
                }}
                aqui23:
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escriba el numero de habilidad que desea usar: ");
                scanf("%i",&nh);
                switch(nh){
                 case 1:
                    if(hh21=1){
                        switch(ph21){
                            case 1:
                                v1=v1-vh21;
                                a1=a1-ah21;
                                break;
                            case 2:
                                aqui212:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh21-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh21-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh21-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui212;
                                }
                                break;
                            case 3:
                                v4=v4-(dh21-a4);
                                v5=v5-(dh21-a5);
                                v6=v6-(dh21-a6);
                                break;
                            case 4:
                                aqui222:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh21;
                                        a1=a1-ah21;
                                        break;
                                    case 2:
                                        v2=v2-vh21;
                                        a2=a2-ah21;
                                        break;
                                    case 3:
                                        v3=v3-vh21;
                                        a3=a3-ah21;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui222;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui23;
                    }
                 break;
                 case 2:
                    if(hh23=1){
                        switch(ph22){
                            case 1:
                                v1=v1-vh22;
                                a1=a1-ah22;
                                break;
                            case 2:
                                aqui221:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh22-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh22-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh22-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui221;
                                }
                                break;
                            case 3:
                                v4=v4-(dh22-a4);
                                v5=v5-(dh22-a5);
                                v6=v6-(dh22-a6);
                                break;
                            case 4:
                                aqui225:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh22;
                                        a1=a1-ah22;
                                        break;
                                    case 2:
                                        v2=v2-vh22;
                                        a2=a2-ah22;
                                        break;
                                    case 3:
                                        v3=v3-vh22;
                                        a3=a3-ah22;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui225;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui23;
                    }
                 break;
                 case 3:
                    if(hh23=1){
                        switch(ph23){
                            case 1:
                                v1=v1-vh23;
                                a1=a1-ah23;
                                break;
                            case 2:
                                aqui231:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh23-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh23-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh23-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui321;
                                }
                                break;
                            case 3:
                                v4=v4-(dh23-a4);
                                v5=v5-(dh23-a5);
                                v6=v6-(dh23-a6);
                                break;
                            case 4:
                                aqui232:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh23;
                                        a1=a1-ah23;
                                        break;
                                    case 2:
                                        v2=v2-vh23;
                                        a2=a2-ah23;
                                        break;
                                    case 3:
                                        v3=v3-vh23;
                                        a3=a3-ah23;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui232;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui23;
                    }
                 break;
                 default:
                 printf("El nuemro que elegiste no es una opcion");
                 system("pause");
                 goto aqui23;
                }
            break;


            case 3:
                whanilidadesgeneral(p3);
                if(la3<4){
                if(xp3>59){
                lvl3=lvl3+1;
                la3=la3+1;
                xp3=0;}
                if(lvl3=1){
                lvl3=0;
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escribe el numero de la habilidad que desees habilitar y presiona enter : ");
                scanf("%i",&nhm);
                switch(nhm){
                case 1:
                    hh31=1;
                    break;
                case 2:
                    hh32=1;
                    break;
                case 3:
                    hh33=1;
                    break;
                default:
                    gotoxy(4,2);
                    printf("Puede que el numero que escogiste no sea una opcion, sea o no es el caso, ");
                    system("pause");
                }
                }}
                aqui33:
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escriba el numero de habilidad que desea usar: ");
                scanf("%i",&nh);
                switch(nh){
                 case 1:
                    if(hh31=1){
                        switch(ph31){
                            case 1:
                                v1=v1-vh31;
                                a1=a1-ah31;
                                break;
                            case 2:
                                aqui315:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh31-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh31-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh31-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui315;
                                }
                                break;
                            case 3:
                                v4=v4-(dh31-a4);
                                v5=v5-(dh31-a5);
                                v6=v6-(dh31-a6);
                                break;
                            case 4:
                                aqui328:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh31;
                                        a1=a1-ah31;
                                        break;
                                    case 2:
                                        v2=v2-vh31;
                                        a2=a2-ah31;
                                        break;
                                    case 3:
                                        v3=v3-vh31;
                                        a3=a3-ah31;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui328;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui33;
                    }
                 break;
                 case 2:
                    if(hh32=1){
                        switch(ph32){
                            case 1:
                                v1=v1-vh32;
                                a1=a1-ah32;
                                break;
                            case 2:
                                aqui321:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh32-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh32-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh32-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui321;
                                }
                                break;
                            case 3:
                                v4=v4-(dh32-a4);
                                v5=v5-(dh32-a5);
                                v6=v6-(dh32-a6);
                                break;
                            case 4:
                                aqui322:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh32;
                                        a1=a1-ah32;
                                        break;
                                    case 2:
                                        v2=v2-vh32;
                                        a2=a2-ah32;
                                        break;
                                    case 3:
                                        v3=v3-vh32;
                                        a3=a3-ah32;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui322;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui33;
                    }
                 break;
                 case 3:
                    if(hh33=1){
                        switch(ph33){
                            case 1:
                                v1=v1-vh33;
                                a1=a1-ah33;
                                break;
                            case 2:
                                aqui331:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-(dh33-a4);
                                        break;
                                    case 2:
                                        v5=v5-(dh33-a5);
                                        break;
                                    case 3:
                                        v6=v6-(dh33-a6);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui331;
                                }
                                break;
                            case 3:
                                v4=v4-(dh33-a4);
                                v5=v5-(dh33-a5);
                                v6=v6-(dh33-a6);
                                break;
                            case 4:
                                aqui332:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-vh33;
                                        a1=a1-ah33;
                                        break;
                                    case 2:
                                        v2=v2-vh33;
                                        a2=a2-ah33;
                                        break;
                                    case 3:
                                        v3=v3-vh33;
                                        a3=a3-ah33;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui332;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui33;
                    }
                 break;
                 default:
                 printf("El nuemro que elegiste no es una opcion");
                 system("pause");
                 goto aqui33;
                }
            break;


        }


    lineaturnoi('0');
        gotoxy(4,2);
        printf("                                                                                                                ");
        color(ROJOB,NEGRO);
        gotoxy(4,2);
        printf("Escriba el numero del campeon con el que desea jugar el turno: ");
        scanf("%i",&nc);

        switch(nc){

            case 1:
                whanilidadesgeneral(p4);
                if(la4<4){
                if(xp4>59){
                lvl4=lvl4+1;
                la4=la4+1;
                xp4=0;}
                if(lvl4=1){
                lvl4=0;
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escribe el numero de la habilidad que desees habilitar y presiona enter : ");
                scanf("%i",&nhm);
                switch(nhm){
                case 1:
                    hh41=1;
                    break;
                case 2:
                    hh42=1;
                    break;
                case 3:
                    hh43=1;
                    break;
                default:
                    gotoxy(4,2);
                    printf("Puede que el numero que escogiste no sea una opcion, sea o no es el caso, ");
                    system("pause");
                }
                }}
                aqui43:
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escriba el numero de habilidad que desea usar: ");
                scanf("%i",&nh);
                switch(nh){
                 case 1:
                    if(hh41=1){
                        switch(ph41){
                            case 1:
                                v4=v4-vh41;
                                a4=a4-ah41;
                                break;
                            case 2:
                                aqui41:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh41-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh41-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh41-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui41;
                                }
                                break;
                            case 3:
                                v1=v1-(dh41-a1);
                                v2=v2-(dh41-a2);
                                v3=v3-(dh41-a3);
                                break;
                            case 4:
                                aqui42:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh41;
                                        a4=a4-ah41;
                                        break;
                                    case 2:
                                        v5=v5-vh41;
                                        a5=a5-ah41;
                                        break;
                                    case 3:
                                        v6=v6-vh41;
                                        a6=a6-ah41;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui42;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui43;
                    }
                 break;
                 case 2:
                    if(hh12=1){
                        switch(ph12){
                            case 1:
                                v4=v4-vh42;
                                a4=a4-ah42;
                                break;
                            case 2:
                                aqui421:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh42-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh42-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh42-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui421;
                                }
                                break;
                            case 3:
                                v1=v1-(dh42-a1);
                                v2=v2-(dh42-a2);
                                v3=v3-(dh42-a3);
                                break;
                            case 4:
                                aqui422:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh42;
                                        a4=a4-ah42;
                                        break;
                                    case 2:
                                        v5=v5-vh42;
                                        a5=a5-ah42;
                                        break;
                                    case 3:
                                        v6=v6-vh42;
                                        a6=a6-ah42;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui422;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui43;
                    }
                 break;
                 case 3:
                    if(hh43=1){
                        switch(ph43){
                            case 1:
                                v4=v4-vh43;
                                a4=a4-ah43;
                                break;
                            case 2:
                                aqui431:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh43-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh43-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh43-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui431;
                                }
                                break;
                            case 3:
                                v1=v4-(dh43-a4);
                                v2=v2-(dh43-a2);
                                v3=v3-(dh43-a3);
                                break;
                            case 4:
                                aqui432:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh43;
                                        a4=a4-ah43;
                                        break;
                                    case 2:
                                        v5=v5-vh43;
                                        a5=a5-ah43;
                                        break;
                                    case 3:
                                        v6=v6-vh43;
                                        a6=a6-ah43;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui432;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui43;
                    }
                 break;
                 default:
                 printf("El nuemro que elegiste no es una opcion");
                 system("pause");
                 goto aqui43;
                }
            break;


             case 2:
                whanilidadesgeneral(p5);
                if(la5<4){
                if(xp5>59){
                lvl5=lvl5+1;
                la5=la5+1;
                xp5=0;}
                if(lvl5=1){
                lvl5=0;
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escribe el numero de la habilidad que desees habilitar y presiona enter : ");
                scanf("%i",&nhm);
                switch(nhm){
                case 1:
                    hh51=1;
                    break;
                case 2:
                    hh52=1;
                    break;
                case 3:
                    hh53=1;
                    break;
                default:
                    gotoxy(4,2);
                    printf("Puede que el numero que escogiste no sea una opcion, sea o no es el caso, ");
                    system("pause");
                }
                }}
                aqui53:
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escriba el numero de habilidad que desea usar: ");
                scanf("%i",&nh);
                switch(nh){
                 case 1:
                    if(hh51=1){
                        switch(ph51){
                            case 1:
                                v5=v5-vh51;
                                a5=a5-ah51;
                                break;
                            case 2:
                                aqui541:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh51-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh51-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh51-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui541;
                                }
                                break;
                            case 3:
                                v1=v1-(dh51-a1);
                                v2=v2-(dh51-a2);
                                v3=v3-(dh51-a3);
                                break;
                            case 4:
                                aqui542:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh51;
                                        a4=a4-ah51;
                                        break;
                                    case 2:
                                        v5=v5-vh51;
                                        a5=a5-ah51;
                                        break;
                                    case 3:
                                        v6=v6-vh51;
                                        a6=a6-ah51;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui542;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui53;
                    }
                 break;
                 case 2:
                    if(hh52=1){
                        switch(ph52){
                            case 1:
                                v5=v5-vh52;
                                a5=a5-ah52;
                                break;
                            case 2:
                                aqui521:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh52-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh52-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh52-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui521;
                                }
                                break;
                            case 3:
                                v1=v1-(dh52-a1);
                                v2=v2-(dh52-a2);
                                v3=v3-(dh52-a3);
                                break;
                            case 4:
                                aqui522:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh52;
                                        a4=a4-ah52;
                                        break;
                                    case 2:
                                        v5=v5-vh52;
                                        a5=a5-ah52;
                                        break;
                                    case 3:
                                        v6=v6-vh52;
                                        a6=a6-ah52;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui522;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui53;
                    }
                 break;
                 case 3:
                    if(hh53=1){
                        switch(ph53){
                            case 1:
                                v5=v5-vh53;
                                a5=a5-ah53;
                                break;
                            case 2:
                                aqui531:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh53-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh53-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh53-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui531;
                                }
                                break;
                            case 3:
                                v1=v4-(dh53-a4);
                                v2=v2-(dh53-a2);
                                v3=v3-(dh53-a3);
                                break;
                            case 4:
                                aqui532:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh53;
                                        a4=a4-ah53;
                                        break;
                                    case 2:
                                        v5=v5-vh53;
                                        a5=a5-ah53;
                                        break;
                                    case 3:
                                        v6=v6-vh53;
                                        a6=a6-ah53;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui532;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui53;
                    }
                 break;
                 default:
                 printf("El nuemro que elegiste no es una opcion");
                 system("pause");
                 goto aqui53;
                }
            break;


             case 3:
                whanilidadesgeneral(p6);
                if(la6<4){
                if(xp6>59){
                lvl6=lvl6+1;
                la6=la6+1;
                xp6=0;}
                if(lvl6=1){
                lvl6=0;
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escribe el numero de la habilidad que desees habilitar y presiona enter : ");
                scanf("%i",&nhm);
                switch(nhm){
                case 1:
                    hh61=1;
                    break;
                case 2:
                    hh62=1;
                    break;
                case 3:
                    hh63=1;
                    break;
                default:
                    gotoxy(4,2);
                    printf("Puede que el numero que escogiste no sea una opcion, sea o no es el caso, ");
                    system("pause");
                }
                }}
                aqui63:
                printf("                                                                                                                ");
                gotoxy(4,2);
                printf("Escriba el numero de habilidad que desea usar: ");
                scanf("%i",&nh);
                switch(nh){
                 case 1:
                    if(hh61=1){
                        switch(ph61){
                            case 1:
                                v6=v6-vh61;
                                a6=a6-ah61;
                                break;
                            case 2:
                                aqui641:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh61-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh61-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh61-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui641;
                                }
                                break;
                            case 3:
                                v1=v1-(dh61-a1);
                                v2=v2-(dh61-a2);
                                v3=v3-(dh61-a3);
                                break;
                            case 4:
                                aqui642:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh61;
                                        a4=a4-ah61;
                                        break;
                                    case 2:
                                        v5=v5-vh61;
                                        a5=a5-ah61;
                                        break;
                                    case 3:
                                        v6=v6-vh61;
                                        a6=a6-ah61;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui642;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui63;
                    }
                 break;
                 case 2:
                    if(hh62=1){
                        switch(ph62){
                            case 1:
                                v6=v6-vh62;
                                a6=a6-ah62;
                                break;
                            case 2:
                                aqui621:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh62-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh62-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh62-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui621;
                                }
                                break;
                            case 3:
                                v1=v1-(dh62-a1);
                                v2=v2-(dh62-a2);
                                v3=v3-(dh62-a3);
                                break;
                            case 4:
                                aqui622:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh62;
                                        a4=a4-ah62;
                                        break;
                                    case 2:
                                        v5=v5-vh62;
                                        a5=a5-ah62;
                                        break;
                                    case 3:
                                        v6=v6-vh62;
                                        a6=a6-ah62;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui622;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui63;
                    }
                 break;
                 case 3:
                    if(hh63=1){
                        switch(ph63){
                            case 1:
                                v6=v6-vh63;
                                a6=a6-ah63;
                                break;
                            case 2:
                                aqui631:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del enemigo con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v1=v1-(dh63-a1);
                                        break;
                                    case 2:
                                        v2=v2-(dh63-a2);
                                        break;
                                    case 3:
                                        v3=v3-(dh63-a3);
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui631;
                                }
                                break;
                            case 3:
                                v1=v4-(dh63-a4);
                                v2=v2-(dh63-a2);
                                v3=v3-(dh63-a3);
                                break;
                            case 4:
                                aqui632:
                                printf("                                                                                                                ");
                                gotoxy(4,2);
                                printf("Escriba el numero del aliado con que desea usarla: ");
                                scanf("%i",&nce);
                                switch(nce){
                                    case 1:
                                        v4=v4-vh63;
                                        a4=a4-ah63;
                                        break;
                                    case 2:
                                        v5=v5-vh63;
                                        a5=a5-ah63;
                                        break;
                                    case 3:
                                        v6=v6-vh63;
                                        a6=a6-ah63;
                                        break;
                                    default:
                                        printf("Solo puedes escoger 1, 2 o 3. ");
                                        system("pause");
                                        goto aqui632;
                                }
                                break;

                }}else{
                    printf("La habiliad que escogiste no esta desbloqueada");
                    system("pause");
                    goto aqui63;
                    }
                 break;
                 default:
                 printf("El nuemro que elegiste no es una opcion");
                 system("pause");
                 goto aqui63;
                }
              break;
        }









    xp1=xp1+10;
    xp2=xp2+12;
    xp3=xp3+15;
    xp4=xp4+9;
    xp5=xp5+13;
    xp6=xp6+16;



    int lolol=1;
    if(v1<1){
        if(v2<1){
            if(v3<1){
                char lololol;
                gotoxy(4,2);
                color(BLANCOB,NEGRO);
                printf("VICTORIA para el equipo rojo ( 2 ), �Deceas continuar(c) o salir (s)?");
                scanf("%c",&lololol);
                if(lololol=='c'){
                    ya=0;
                }else{
                    FILE* menur8 = fopen("menuretorno.txt","w"); //dejar en blanco menu.txt
                    fputc('a',menur8);
                    fclose(menur8);
                }
            }
        }
    }


}while(ya!=0);
}

void whanilidadesgeneral(int p){
    if(p=1){whabilidadesk('0');}
    if(p=2){whabilidadesm('0');}
    if(p=3){whabilidadess('0');}
    if(p=4){whabilidadesh('0');}
    if(p=5){whabilidadest('0');}
    if(p=6){whabilidadesd('0');}
}

void borrarhabilidades(char i){
    color(BLANCOB,NEGRO);
    int x=4;

    gotoxy(x,31);
    printf("                                       ");
    gotoxy(x,32);
    printf("                                       ");
    gotoxy(x,33);
    printf("                                       ");
    gotoxy(x,34);
    printf("                                       ");
    gotoxy(x,35);
    printf("                                       ");
    gotoxy(x,36);
    printf("                                       ");
    gotoxy(x,37);
    printf("                                       ");
    gotoxy(x,38);
    printf("                                       ");
    gotoxy(x,39);
    printf("                                       ");

    x=44;
    gotoxy(x,31);
    printf("                                       ");
    gotoxy(x,32);
    printf("                                       ");
    gotoxy(x,33);
    printf("                                       ");
    gotoxy(x,34);
    printf("                                       ");
    gotoxy(x,35);
    printf("                                       ");
    gotoxy(x,36);
    printf("                                       ");
    gotoxy(x,37);
    printf("                                       ");
    gotoxy(x,38);
    printf("                                       ");
    gotoxy(x,39);
    printf("                                       ");

    x=84;
    gotoxy(x,31);
    printf("                                       ");
    gotoxy(x,32);
    printf("                                       ");
    gotoxy(x,33);
    printf("                                       ");
    gotoxy(x,34);
    printf("                                       ");
    gotoxy(x,35);
    printf("                                       ");
    gotoxy(x,36);
    printf("                                       ");
    gotoxy(x,37);
    printf("                                       ");
    gotoxy(x,38);
    printf("                                       ");
    gotoxy(x,39);
    printf("                                       ");

}

void borrarrol(char i){
    color(BLANCOB,NEGRO);
    int x=124;
    gotoxy(x,31);
    printf("                                       ");
    gotoxy(x,32);
    printf("                                       ");
    gotoxy(x,33);
    printf("                                       ");
    gotoxy(x,34);
    printf("                                       ");
    gotoxy(x,35);
    printf("                                       ");
    gotoxy(x,36);
    printf("                                       ");
    gotoxy(x,37);
    printf("                                       ");
    gotoxy(x,38);
    printf("                                       ");
    gotoxy(x,39);
    printf("                                       ");
}

void whabilidadesk(char i){
borrarhabilidades('0');
    FILE* champelegido = fopen("h1k.txt","r");
    char whab1[300];
    int y=32,x=4+4;
    gotoxy(7,y);
    do{
        fgets(whab1,300,champelegido);
        gotoxy(x,y);
        printf("%s",whab1);
        y=y+1;
    }while(!feof(champelegido));
    fclose(champelegido);

    FILE* champelegidod = fopen("h2k.txt","r");
    char whab2[300];
    y=32,x=44+4;
    gotoxy(7,y);
    do{
        fgets(whab2,300,champelegidod);
        gotoxy(x,y);
        printf("%s",whab2);
        y=y+1;
    }while(!feof(champelegidod));
    fclose(champelegidod);

    FILE* champelegidot = fopen("h3k.txt","r");
    char whab3[300];
    y=32,x=84+4;
    gotoxy(7,y);
    do{
        fgets(whab3,300,champelegidot);
        gotoxy(x,y);
        printf("%s",whab3);
        y=y+1;
    }while(!feof(champelegidot));
    fclose(champelegidot);
}

void whabilidadesm(char i){
borrarhabilidades('0');

    FILE* champelegido = fopen("h1m.txt","r");
    char whab1[300];
    int y=32,x=4+4;
    gotoxy(7,y);
    do{
        fgets(whab1,300,champelegido);
        gotoxy(x,y);
        printf("%s",whab1);
        y=y+1;
    }while(!feof(champelegido));
    fclose(champelegido);

    FILE* champelegidod = fopen("h2m.txt","r");
    char whab2[300];
    y=32,x=44+4;
    gotoxy(7,y);
    do{
        fgets(whab2,300,champelegidod);
        gotoxy(x,y);
        printf("%s",whab2);
        y=y+1;
    }while(!feof(champelegidod));
    fclose(champelegidod);

    FILE* champelegidot = fopen("h3m.txt","r");
    char whab3[300];
    y=32,x=84+4;
    gotoxy(7,y);
    do{
        fgets(whab3,300,champelegidot);
        gotoxy(x,y);
        printf("%s",whab3);
        y=y+1;
    }while(!feof(champelegidot));
    fclose(champelegidot);
}

void whabilidadess(char i){
borrarhabilidades('0');
    FILE* champelegido = fopen("h1s.txt","r");
    char whab1[300];
    int y=32,x=4+4;
    gotoxy(7,y);
    do{
        fgets(whab1,300,champelegido);
        gotoxy(x,y);
        printf("%s",whab1);
        y=y+1;
    }while(!feof(champelegido));
    fclose(champelegido);

    FILE* champelegidod = fopen("h2s.txt","r");
    char whab2[300];
    y=32,x=44+4;
    gotoxy(7,y);
    do{
        fgets(whab2,300,champelegidod);
        gotoxy(x,y);
        printf("%s",whab2);
        y=y+1;
    }while(!feof(champelegidod));
    fclose(champelegidod);

    FILE* champelegidot = fopen("h3s.txt","r");
    char whab3[300];
    y=32,x=84+4;
    gotoxy(7,y);
    do{
        fgets(whab3,300,champelegidot);
        gotoxy(x,y);
        printf("%s",whab3);
        y=y+1;
    }while(!feof(champelegidot));
    fclose(champelegidot);
}

void whabilidadesh(char i){
borrarhabilidades('0');
    FILE* champelegido = fopen("h1h.txt","r");
    char whab1[300];
    int y=32,x=4+4;
    gotoxy(7,y);
    do{
        fgets(whab1,300,champelegido);
        gotoxy(x,y);
        printf("%s",whab1);
        y=y+1;
    }while(!feof(champelegido));
    fclose(champelegido);

    FILE* champelegidod = fopen("h2h.txt","r");
    char whab2[300];
    y=32,x=44+4;
    gotoxy(7,y);
    do{
        fgets(whab2,300,champelegidod);
        gotoxy(x,y);
        printf("%s",whab2);
        y=y+1;
    }while(!feof(champelegidod));
    fclose(champelegidod);

    FILE* champelegidot = fopen("h3h.txt","r");
    char whab3[300];
    y=32,x=84+4;
    gotoxy(7,y);
    do{
        fgets(whab3,300,champelegidot);
        gotoxy(x,y);
        printf("%s",whab3);
        y=y+1;
    }while(!feof(champelegidot));
    fclose(champelegidot);
}

void whabilidadest(char i){
borrarhabilidades('0');
    FILE* champelegido = fopen("h1t.txt","r");
    char whab1[300];
    int y=32,x=4+4;
    gotoxy(7,y);
    do{
        fgets(whab1,300,champelegido);
        gotoxy(x,y);
        printf("%s",whab1);
        y=y+1;
    }while(!feof(champelegido));
    fclose(champelegido);

    FILE* champelegidod = fopen("h2t.txt","r");
    char whab2[300];
    y=32,x=44+4;
    gotoxy(7,y);
    do{
        fgets(whab2,300,champelegidod);
        gotoxy(x,y);
        printf("%s",whab2);
        y=y+1;
    }while(!feof(champelegidod));
    fclose(champelegidod);

    FILE* champelegidot = fopen("h3t.txt","r");
    char whab3[300];
    y=32,x=84+4;
    gotoxy(7,y);
    do{
        fgets(whab3,300,champelegidot);
        gotoxy(x,y);
        printf("%s",whab3);
        y=y+1;
    }while(!feof(champelegidot));
    fclose(champelegidot);
}

void whabilidadesd(char i){
borrarhabilidades('0');
    FILE* champelegido = fopen("h1d.txt","r");
    char whab1[300];
    int y=32,x=4+4;
    gotoxy(7,y);
    do{
        fgets(whab1,300,champelegido);
        gotoxy(x,y);
        printf("%s",whab1);
        y=y+1;
    }while(!feof(champelegido));
    fclose(champelegido);

    FILE* champelegidod = fopen("h2d.txt","r");
    char whab2[300];
    y=32,x=44+4;
    gotoxy(7,y);
    do{
        fgets(whab2,300,champelegidod);
        gotoxy(x,y);
        printf("%s",whab2);
        y=y+1;
    }while(!feof(champelegidod));
    fclose(champelegidod);

    FILE* champelegidot = fopen("h3d.txt","r");
    char whab3[300];
    y=32,x=84+4;
    gotoxy(7,y);
    do{
        fgets(whab3,300,champelegidot);
        gotoxy(x,y);
        printf("%s",whab3);
        y=y+1;
    }while(!feof(champelegidot));
    fclose(champelegidot);
}

void seleccionchamps(char i){
    int nc=1, nce=1;
    FILE* punok = fopen("p1.txt","w");
    FILE* punom = fopen("p1.txt","w");
    FILE* punos = fopen("p1.txt","w");
    FILE* punoh = fopen("p1.txt","w");
    FILE* punot = fopen("p1.txt","w");
    FILE* punod = fopen("p1.txt","w");

    FILE* pdosk = fopen("p2.txt","w");
    FILE* pdosm = fopen("p2.txt","w");
    FILE* pdoss = fopen("p2.txt","w");
    FILE* pdosh = fopen("p2.txt","w");
    FILE* pdost = fopen("p2.txt","w");
    FILE* pdosd = fopen("p2.txt","w");

    FILE* ptresk = fopen("p3.txt","w");
    FILE* ptresm = fopen("p3.txt","w");
    FILE* ptress = fopen("p3.txt","w");
    FILE* ptresh = fopen("p3.txt","w");
    FILE* ptrest = fopen("p3.txt","w");
    FILE* ptresd = fopen("p3.txt","w");

    FILE* punokk = fopen("p4.txt","w");
    FILE* punomm = fopen("p4.txt","w");
    FILE* punoss = fopen("p4.txt","w");
    FILE* punohh = fopen("p4.txt","w");
    FILE* punott = fopen("p4.txt","w");
    FILE* punodd = fopen("p4.txt","w");

    FILE* pdoskk = fopen("p5.txt","w");
    FILE* pdosmm = fopen("p5.txt","w");
    FILE* pdosss = fopen("p5.txt","w");
    FILE* pdoshh = fopen("p5.txt","w");
    FILE* pdostt = fopen("p5.txt","w");
    FILE* pdosdd = fopen("p5.txt","w");

    FILE* ptreskk = fopen("p6.txt","w");
    FILE* ptresmm = fopen("p6.txt","w");
    FILE* ptresss = fopen("p6.txt","w");
    FILE* ptrestt = fopen("p6.txt","w");
    FILE* ptreshh = fopen("p6.txt","w");
    FILE* ptresdd = fopen("p6.txt","w");

    while(nc!=0){
        gotoxy(4,2);
        printf("                                                                                                           ");
        nc=1;
                                                                                                                                                  //
    if(nce=1){if(nc=1){     //p1

    char ele='a';
    gotoxy(4,2);
    color(CYANB,NEGRO);
    printf("Escribe la letra del primer campeon que formara parte del equipo 1 : ");
    scanf("%c",&ele);
    switch(ele){
        case 'k':
             //dejar en blanco pi.txt
            fputc('k',punok);
            fclose(punok);
            nc=nc+1;
            break;
        case 'm':
             //dejar en blanco pi.txt
            fputc('k',punom);
            fclose(punom);
            nc=nc+1;
            break;
        case 's':
             //dejar en blanco pi.txt
            fputc('k',punos);
            fclose(punos);
            nc=nc+1;
            break;
        case 'h':
             //dejar en blanco pi.txt
            fputc('k',punoh);
            fclose(punoh);
            nc=nc+1;
            break;
        case 't':
             //dejar en blanco pi.txt
            fputc('k',punot);
            fclose(punot);
            nc=nc+1;
            break;
        case 'd':
             //dejar en blanco pi.txt
            fputc('k',punod);
            fclose(punod);
            nc=nc+1;
            break;
        default:
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            }}}
gotoxy(4,2);
        printf("                                                                                                                   ");
nce=1;

    if(nce=1){if(nc=2){     //p2
    char ele='a';
    gotoxy(4,2);
    color(CYANB,NEGRO);
    printf("Escribe la letra del segundo campeon que formara parte del equipo 1 : ");
    scanf("%c",&ele);

    FILE* eleante = fopen("p1.txt","r");
    int ele2 = fgetc(eleante);
    if(ele2!=ele){

    switch(ele){
        case 'k':
             //dejar en blanco p2.txt
            fputc('k',pdosk);
            fclose(pdosk);
            nc=nc+1;
            break;
        case 'm':
             //dejar en blanco p2.txt
            fputc('k',pdosm);
            fclose(pdosm);
            nc=nc+1;
            break;
        case 's':
             //dejar en blanco p2.txt
            fputc('k',pdoss);
            fclose(pdoss);
            nc=nc+1;
            break;
        case 'h':
             //dejar en blanco p2.txt
            fputc('k',pdosh);
            fclose(pdosh);
            nc=nc+1;
            break;
        case 't':
             //dejar en blanco p2.txt
            fputc('k',pdost);
            fclose(pdost);
            nc=nc+1;
            break;
        case 'd':
             //dejar en blanco p2.txt
            fputc('k',pdosd);
            fclose(pdosd);
            nc=nc+1;
            break;
        default:
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso ");
            system("pause");
            }}else{
            gotoxy(4,2);
            printf("No se pueden repetir campeones. ");
            system("pause");
            }fclose(eleante);
            }}
gotoxy(4,2);
        printf("                                                                                                                   ");
        nce=1;

    if(nce=1){if(nc=3){     //p3
    char ele='a';
    gotoxy(4,2);
    color(CYANB,NEGRO);
    printf("Escribe la letra del tercer campeon que formara parte del equipo 1 : ");
    scanf("%c",&ele);

    FILE* eleantee = fopen("p1.txt","r");
    int ele2 = fgetc(eleantee);
    if(ele2!=ele){

         FILE* elepass = fopen("p2.txt","r");
         int ele3 = fgetc(elepass);
         if(ele3!=ele){

    switch(ele){
        case 'k':
             //dejar en blanco p2.txt
            fputc('k',ptresk);
            fclose(ptresk);
            nc=1;
            nce=2;
            break;
        case 'm':
             //dejar en blanco p2.txt
            fputc('k',ptresm);
            fclose(ptresm);
            nc=1;
            nce=2;
            break;
        case 's':
             //dejar en blanco p2.txt
            fputc('k',ptress);
            fclose(ptress);
            nc=1;
            nce=2;
            break;
        case 'h':
             //dejar en blanco p2.txt
            fputc('k',ptresh);
            fclose(ptresh);
            nc=1;
            nce=2;
            break;
        case 't':
             //dejar en blanco p2.txt
            fputc('k',ptrest);
            fclose(ptrest);
            nc=1;
            nce=2;
            break;
        case 'd':
             //dejar en blanco p2.txt
            fputc('k',ptresd);
            fclose(ptresd);
            nc=1;
            nce=2;
            break;
        default:
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso ");
            system("pause");
            }}
            else{
            gotoxy(4,2);
            printf("No se pueden repetir campeones. ");
            system("pause");
            }fclose(elepass);}else{
            gotoxy(4,2);
            printf("No se pueden repetir campeones. ");
            system("pause");
            }fclose(eleantee);
    }}
gotoxy(4,2);
        printf("                                                                                                                   ");


    if(nce=2){if(nc=1){     //p4
    char ele='a';
    gotoxy(4,2);
    color(ROJOB,NEGRO);
    printf("Escribe la letra del primer campeon que formara parte del equipo 2 : ");
    scanf("%c",&ele);
    switch(ele){
        case 'k':
             //dejar en blanco pi.txt
            fputc('k',punokk);
            fclose(punokk);
            nc=nc+1;
            break;
        case 'm':
             //dejar en blanco pi.txt
            fputc('k',punomm);
            fclose(punomm);
            nc=nc+1;
            break;
        case 's':
             //dejar en blanco pi.txt
            fputc('k',punoss);
            fclose(punoss);
            nc=nc+1;
            break;
        case 'h':
             //dejar en blanco pi.txt
            fputc('k',punohh);
            fclose(punohh);
            nc=nc+1;
            break;
        case 't':
             //dejar en blanco pi.txt
            fputc('k',punott);
            fclose(punott);
            nc=nc+1;
            break;
        case 'd':
             //dejar en blanco pi.txt
            fputc('k',punodd);
            fclose(punodd);
            nc=nc+1;
            break;
        default:
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            }}}
gotoxy(4,2);
        printf("                                                                                                                   ");


    if(nce=2){if(nc=2){     //p5
    char ele='a';
    gotoxy(4,2);
    color(ROJOB,NEGRO);
    printf("Escribe la letra del segundo campeon que formara parte del equipo 2 : ");
    scanf("%c",&ele);

    FILE* eeleantes = fopen("p4.txt","r");
    int ele2 = fgetc(eeleantes);
    if(ele2!=ele){

    switch(ele){
        case 'k':
             //dejar en blanco p2.txt
            fputc('k',pdoskk);
            fclose(pdoskk);
            nc=nc+1;
            break;
        case 'm':
             //dejar en blanco p2.txt
            fputc('k',pdosmm);
            fclose(pdosmm);
            nc=nc+1;
            break;
        case 's':
             //dejar en blanco p2.txt
            fputc('k',pdosss);
            fclose(pdosss);
            nc=nc+1;
            break;
        case 'h':
             //dejar en blanco p2.txt
            fputc('k',pdoshh);
            fclose(pdoshh);
            nc=nc+1;
            break;
        case 't':
             //dejar en blanco p2.txt
            fputc('k',pdostt);
            fclose(pdostt);
            nc=nc+1;
            break;
        case 'd':
             //dejar en blanco p2.txt
            fputc('k',pdosdd);
            fclose(pdosdd);
            nc=nc+1;
            break;
        default:
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso ");
            system("pause");
            }}else{
            gotoxy(4,2);
            printf("No se pueden repetir campeones. ");
            system("pause");
            }fclose(eeleantes);
            }}
gotoxy(4,2);
        printf("                                                                                                                   ");


    if(nce=2){if(nc=3){     //p6
    char ele='a';
    gotoxy(4,2);
    color(ROJOB,NEGRO);
    printf("Escribe la letra del tercer campeon que formara parte del equipo 2 : ");
    scanf("%c",&ele);

    FILE* eeeleante = fopen("p4.txt","r");
    int ele2 = fgetc(eeeleante);
    if(ele2!=ele){

         FILE* eeelepas = fopen("p5.txt","r");
         int ele3 = fgetc(eeelepas);
         if(ele3!=ele){

    switch(ele){
        case 'k':
             //dejar en blanco p2.txt
            fputc('k',ptreskk);
            fclose(ptreskk);
            nc=1;
            nce=0;
            break;
        case 'm':
             //dejar en blanco p2.txt
            fputc('k',ptresmm);
            fclose(ptresmm);
            nc=1;
            nce=0;
            break;
        case 's':
             //dejar en blanco p2.txt
            fputc('k',ptresss);
            fclose(ptresss);
            nc=1;
            nce=0;
            break;
        case 'h':
             //dejar en blanco p2.txt
            fputc('k',ptreshh);
            fclose(ptreshh);
            nc=1;
            nce=0;
            break;
        case 't':
             //dejar en blanco p2.txt
            fputc('k',ptrestt);
            fclose(ptrestt);
            nc=1;
            nce=0;
            break;
        case 'd':
             //dejar en blanco p2.txt
            fputc('k',ptresdd);
            fclose(ptresdd);
            nc=1;
            nce=0;
            break;
        default:
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso ");
            system("pause");
            }}
            else{
            gotoxy(4,2);
            printf("No se pueden repetir campeones. ");
            system("pause");
            }fclose(eeelepas);}else{
            gotoxy(4,2);
            printf("No se pueden repetir campeones. ");
            system("pause");
            }fclose(eeeleante);
    }}
gotoxy(4,2);
        printf("                                                                                                                   ");

    }

    fclose(punok);
    fclose(punom);
    fclose(punos);
    fclose(punoh);
    fclose(punot);
    fclose(punod);

    fclose(pdosk);
    fclose(pdosm);
    fclose(pdoss);
    fclose(pdosh);
    fclose(pdost);
    fclose(pdosd);

    fclose(ptresk);
    fclose(ptresm);
    fclose(ptress);
    fclose(ptresh);
    fclose(ptrest);
    fclose(ptresd);

    fclose(punokk);
    fclose(punomm);
    fclose(punoss);
    fclose(punohh);
    fclose(punott);
    fclose(punodd);

    fclose(pdoskk);
    fclose(pdosmm);
    fclose(pdosss);
    fclose(pdoshh);
    fclose(pdostt);
    fclose(pdosdd);

    fclose(ptreskk);
    fclose(ptresmm);
    fclose(ptresss);
    fclose(ptrestt);
    fclose(ptreshh);
    fclose(ptresdd);
}

void wopcionesdechamps(char i){
    color(ROJO,NEGRO);
    simbopersonaje(5,22,142);     //kaisa
    gotoxy(23,16);
    color(NEGRO,BLANCOB);
    printf("  K  A  I  S  A  ");
    gotoxy(26,18);
    color(VERDEB,NEGRO);
    printf("190 de VIDA");
    gotoxy(25,19);
    color(BLANCO,NEGRO);
    printf("0 de ARMADURA");
    gotoxy(25,21);
    color(BLANCOB,NEGRO);
    printf("   (  k  )   ");

    color(ROJO,NEGRO);
    simbopersonaje(59,22,190);     //morde
    gotoxy(69,16);
    color(NEGRO,BLANCOB);
    printf("  M  O  R  D  E  K  A  I  S  E  R  ");
    gotoxy(81,18);
    color(VERDEB,NEGRO);
    printf("190 de VIDA");
    gotoxy(80,19);
    color(BLANCO,NEGRO);
    printf("0 de ARMADURA");
    gotoxy(80,21);
    color(BLANCOB,NEGRO);
    printf("   (  m  )   ");

    color(ROJO,NEGRO);
    simbopersonaje(113,22,208);     //serafine
    gotoxy(127,16);
    color(NEGRO,BLANCOB);
    printf("  S  E  R  A  F  I  N  E  ");
    gotoxy(134,18);
    color(VERDEB,NEGRO);
    printf("190 de VIDA");
    gotoxy(133,19);
    color(BLANCO,NEGRO);
    printf("0 de ARMADURA");
    gotoxy(133,21);
    color(BLANCOB,NEGRO);
    printf("   (  s  )   ");

    color(ROJO,NEGRO);
    simbopersonaje(5,31,159);     //heimer
    gotoxy(15,25);
    color(NEGRO,BLANCOB);
    printf("  H  E  I  M  E  R  D  I  N  G  E  R  ");
    gotoxy(27,27);
    color(VERDEB,NEGRO);
    printf("190 de VIDA");
    gotoxy(26,28);
    color(BLANCO,NEGRO);
    printf("0 de ARMADURA");
    gotoxy(26,30);
    color(BLANCOB,NEGRO);
    printf("   (  h  )   ");

    color(ROJO,NEGRO);
    simbopersonaje(59,31,207);     //tanke
    gotoxy(75,25);
    color(NEGRO,BLANCOB);
    printf("  T  A  N  K  E  C  H  ");
    gotoxy(81,27);
    color(VERDEB,NEGRO);
    printf("190 de VIDA");
    gotoxy(80,28);
    color(BLANCO,NEGRO);
    printf("0 de ARMADURA");
    gotoxy(80,30);
    color(BLANCOB,NEGRO);
    printf("   (  t  )   ");

    color(ROJO,NEGRO);
    simbopersonaje(113,31,158);     //draco
    gotoxy(131,25);
    color(NEGRO,BLANCOB);
    printf("  D  R  A  C  O  ");
    gotoxy(134,27);
    color(VERDEB,NEGRO);
    printf("190 de VIDA");
    gotoxy(133,28);
    color(BLANCO,NEGRO);
    printf("0 de ARMADURA");
    gotoxy(133,30);
    color(BLANCOB,NEGRO);
    printf("   (  d  )   ");

    color(BLANCOB,NEGRO);

}

void selechamps(char i){
    int p1=2;

    color(CYANB,NEGRO);
    while(p1!=1){      //p1
    p1=2;
    char sel;
    FILE* puno = fopen("p1.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del primer campeon que formara parte del equipo 1 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'k':
            fputc('k',puno);
            p1=1;
            break;
        case 'm':
            fputc('m',puno);
            p1=1;
            break;
        case 's':
            fputc('s',puno);
            p1=1;
            break;
        case 'h':
            fputc('h',puno);
            p1=1;
            break;
        case 't':
            fputc('t',puno);
            p1=1;
            break;
        case 'd':
            fputc('d',puno);
            p1=1;
            break;
        default:
            p1=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
    }
    fclose(puno);
    }


    int p2=2;
    while(p2!=1){      //p2
    p2=2;
    char sel;
    FILE* pdos = fopen("p2.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                  ");
    gotoxy(4,2);
    printf("Escribe la letra del segundo campeon que formara parte del equipo 1 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'k':
            fputc('k',pdos);
            p2=1;
            break;
        case 'm':
            fputc('m',pdos);
            p2=1;
            break;
        case 's':
            fputc('s',pdos);
            p2=1;
            break;
        case 'h':
            fputc('h',pdos);
            p2=1;
            break;
        case 't':
            fputc('t',pdos);
            p2=1;
            break;
        case 'd':
            fputc('d',pdos);
            p2=1;
            break;
        default:
            p2=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");

    }
    fclose(pdos);
    }


    int p3=2;
    while(p3!=1){      //p3
    p3=2;
    char sel;
    FILE* ptre = fopen("p3.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del tercer campeon que formara parte del equipo 1 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'k':
            fputc('k',ptre);
            p3=1;
            break;
        case 'm':
            fputc('m',ptre);
            p3=1;
            break;
        case 's':
            fputc('s',ptre);
            p3=1;
            break;
        case 'h':
            fputc('h',ptre);
            p3=1;
            break;
        case 't':
            fputc('t',ptre);
            p3=1;
            break;
        case 'd':
            fputc('d',ptre);
            p3=1;
            break;
        default:
            p3=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");

    }
    fclose(ptre);
    }


    color(ROJOB,NEGRO);
    int p4=2;
    while(p4!=1){      //p4
    p4=2;
    char sel;
    FILE* pcua = fopen("p4.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del primer campeon que formara parte del equipo 2 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'k':
            fputc('k',pcua);
            p4=1;
            break;
        case 'm':
            fputc('m',pcua);
            p4=1;
            break;
        case 's':
            fputc('s',pcua);
            p4=1;
            break;
        case 'h':
            fputc('h',pcua);
            p4=1;
            break;
        case 't':
            fputc('t',pcua);
            p4=1;
            break;
        case 'd':
            fputc('d',pcua);
            p4=1;
            break;
        default:
            p4=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");

    }
    fclose(pcua);
    }


    int p5=2;
    while(p5!=1){      //p5
    p5=2;
    char sel;
    FILE* pcin = fopen("p5.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del segundo campeon que formara parte del equipo 2 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'k':
            fputc('k',pcin);
            p5=1;
            break;
        case 'm':
            fputc('m',pcin);
            p5=1;
            break;
        case 's':
            fputc('s',pcin);
            p5=1;
            break;
        case 'h':
            fputc('h',pcin);
            p5=1;
            break;
        case 't':
            fputc('t',pcin);
            p5=1;
            break;
        case 'd':
            fputc('d',pcin);
            p5=1;
            break;
        default:
            p5=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");

    }
    fclose(pcin);
    }


    int p6=2;
    while(p6!=1){      //p6
    p6=2;
    char sel;
    FILE* psei = fopen("p6.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del tercer campeon que formara parte del equipo 2 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'k':
            fputc('k',psei);
            p6=1;
            break;
        case 'm':
            fputc('m',psei);
            p6=1;
            break;
        case 's':
            fputc('s',psei);
            p6=1;
            break;
        case 'h':
            fputc('h',psei);
            p6=1;
            break;
        case 't':
            fputc('t',psei);
            p6=1;
            break;
        case 'd':
            fputc('d',psei);
            p6=1;
            break;
        default:
            p6=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");

    }
    fclose(psei);
    }

    color(BLANCOB,NEGRO);
}

void seleroles(char i){

    color(CYANB,NEGRO);
    int r1=2;
    while(r1!=1){      //r1
    r1=2;
    char sel;
    FILE* runo = fopen("r1.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del rol que tendra el primer campeon del equipo 1 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'v':
            fputc('v',runo);  //vampiro
            r1=1;
            break;
        case 't':
            fputc('t',runo); //taxi
            r1=1;
            break;
        case 'p':
            fputc('p',runo);  //ptrd
            r1=1;
            break;
        case 'g':
            fputc('g',runo);  //guard
            r1=1;
            break;
        case 'e':
            fputc('e',runo);  //gen i o
            r1=1;
            break;
        case 'a':
            fputc('a',runo); //as
            r1=1;
            break;
        default:
            r1=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            break;
    }
    fclose(runo);
    }

    int r2=2;
    while(r2!=1){      //r2
    r2=2;
    char sel;
    FILE* rdos = fopen("r2.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del rol que tendra el segundo campeon del equipo 1 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'v':
            fputc('v',rdos);
            r2=1;
            break;
        case 't':
            fputc('t',rdos);
            r2=1;
            break;
        case 'p':
            fputc('p',rdos);
            r2=1;
            break;
        case 'g':
            fputc('g',rdos);
            r2=1;
            break;
        case 'e':
            fputc('e',rdos);
            r2=1;
            break;
        case 'a':
            fputc('a',rdos);
            r2=1;
            break;
        default:
            r2=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            break;
    }
    fclose(rdos);
    }

    int r3=2;
    while(r3!=1){      //r3
    r3=2;
    char sel;
    FILE* rtre = fopen("r3.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del rol que tendra el tercer campeon del equipo 1 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'v':
            fputc('v',rtre);
            r3=1;
            break;
        case 't':
            fputc('t',rtre);
            r3=1;
            break;
        case 'p':
            fputc('p',rtre);
            r3=1;
            break;
        case 'g':
            fputc('g',rtre);
            r3=1;
            break;
        case 'e':
            fputc('e',rtre);
            r3=1;
            break;
        case 'a':
            fputc('a',rtre);
            r3=1;
            break;
        default:
            r3=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            break;
    }
    fclose(rtre);
    }

    color(ROJOB,NEGRO);
    int r4=2;
    while(r4!=1){      //r4
    r4=2;
    char sel;
    FILE* rcua = fopen("r4.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del rol que tendra el primer campeon del equipo 2 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'v':
            fputc('v',rcua);
            r4=1;
            break;
        case 't':
            fputc('t',rcua);
            r4=1;
            break;
        case 'p':
            fputc('p',rcua);
            r4=1;
            break;
        case 'g':
            fputc('g',rcua);
            r4=1;
            break;
        case 'e':
            fputc('e',rcua);
            r4=1;
            break;
        case 'a':
            fputc('a',rcua);
            r4=1;
            break;
        default:
            r4=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            break;
    }
    fclose(rcua);
    }

    int r5=2;
    while(r5!=1){      //r5
    r5=2;
    char sel;
    FILE* rcin = fopen("r5.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del rol que tendra el segundo campeon del equipo 2 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'v':
            fputc('v',rcin);
            r5=1;
            break;
        case 't':
            fputc('t',rcin);
            r5=1;
            break;
        case 'p':
            fputc('p',rcin);
            r5=1;
            break;
        case 'g':
            fputc('g',rcin);
            r5=1;
            break;
        case 'e':
            fputc('e',rcin);
            r5=1;
            break;
        case 'a':
            fputc('a',rcin);
            r5=1;
            break;
        default:
            r5=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            break;
    }
    fclose(rcin);
    }

    int r6=2;
    while(r6!=1){      //r6
    r6=2;
    char sel;
    FILE* rsei = fopen("r6.txt","w");
    gotoxy(4,2);
    printf("                                                                                                                 ");
    gotoxy(4,2);
    printf("Escribe la letra del rol que tendra el tercer campeon del equipo 2 : ");
    scanf("%c",&sel);
    switch(sel){
        case 'v':
            fputc('v',rsei);
            r6=1;
            break;
        case 't':
            fputc('t',rsei);
            r6=1;
            break;
        case 'p':
            fputc('p',rsei);
            r6=1;
            break;
        case 'g':
            fputc('g',rsei);
            r6=1;
            break;
        case 'e':
            fputc('e',rsei);
            r6=1;
            break;
        case 'a':
            fputc('a',rsei);
            r6=1;
            break;
        default:
            r6=2;
            gotoxy(4,2);
            printf("Puede que la letra que escogiste no sea una opcion, sea o no es el caso, ");
            system("pause");
            break;
    }
    fclose(rsei);
    }

    color(BLANCOB,NEGRO);
}

void wopcionesderol(char i){

    gotoxy(18,16);
    color(ROJO,CYAN);
    printf("  V  A  M  P  I  R  O  ");
    gotoxy(6,18);
    color(MAGENTA,NEGRO);
    printf("           Convierte el DA�O en VIDA");
    gotoxy(6,19);
    color(ROJOB,NEGRO);
    printf("      Da la misma VIDA ganada en un aliado");
    gotoxy(26,21);
    color(BLANCOB,NEGRO);
    printf("(  v  )");


    gotoxy(65,16);
    color(BLANCO,CYAN);
    printf("  T  A  X  I  D  E  R  M  I  S  T  A  ");
    gotoxy(60,18);
    color(MAGENTA,NEGRO);
    printf("         Convierte el DA�O en ARMADURA");
    gotoxy(60,19);
    color(ROJOB,NEGRO);
    printf("    Da la misma ARMADURA ganada en un aliado");
    gotoxy(80,21);
    color(BLANCOB,NEGRO);
    printf("(  t  )");


    gotoxy(131,16);
    color(CYANB,CYAN);
    printf("  P  T  R  D  ");
    gotoxy(114,18);
    color(MAGENTA,NEGRO);
    printf("  El DA�O ignora parte de la armadura enemiga");
    gotoxy(114,19);
    color(ROJOB,NEGRO);
    printf("        El DA�O ignora toda la ARMADURA");
    gotoxy(134,21);
    color(BLANCOB,NEGRO);
    printf("(  p  )");


    gotoxy(17,25);
    color(VERDEB,CYAN);
    printf("  G  U  A  R  D  I  A  N  ");
    gotoxy(6,27);
    color(MAGENTA,NEGRO);
    printf("          Da VIDA a aliados cada turno");
    gotoxy(6,28);
    color(ROJOB,NEGRO);
    printf("     Protege a aliados del siguiente ataque");
    gotoxy(26,30);
    color(BLANCOB,NEGRO);
    printf("(  g  )");


    gotoxy(75,25);
    color(AMARILLOB,CYAN);
    printf("  G  E  N  I  O  ");
    gotoxy(60,27);
    color(MAGENTA,NEGRO);
    printf("                 Duplica su XP");
    gotoxy(60,28);
    color(ROJOB,NEGRO);
    printf("            Sube un NIVEL a un aliado");
    gotoxy(80,30);
    color(BLANCOB,NEGRO);
    printf("(  e  )");


    gotoxy(126,25);
    color(NEGRO,CYAN);
    printf("  A  S  E  S  I  N  O  ");
    gotoxy(112,27);
    color(MAGENTA,NEGRO);
    printf("              Aumenta un poco su DA�O");
    gotoxy(112,28);
    color(ROJOB,NEGRO);
    printf("         Aumenta bastante su siguiente DA�O");
    gotoxy(132,30);
    color(BLANCOB,NEGRO);
    printf("(  a  )");

    color(BLANCOB,NEGRO);

}

void wrolestv(char i){
    borrarrol('0');
    FILE* rolelegidov = fopen("rv.txt","r");
    char wrolv[300];
    int y=32,x=124+4;
    gotoxy(7,y);
    do{
        fgets(wrolv,300,rolelegidov);
        gotoxy(x,y);
        printf("%s",wrolv);
        y=y+1;
    }while(!feof(rolelegidov));
    fclose(rolelegidov);
}

void wrolestt(char i){
    borrarrol('0');
    FILE* rolelegidot = fopen("rt.txt","r");
    char wrolt[300];
    int y=32,x=124+4;
    gotoxy(7,y);
    do{
        fgets(wrolt,300,rolelegidot);
        gotoxy(x,y);
        printf("%s",wrolt);
        y=y+1;
    }while(!feof(rolelegidot));
    fclose(rolelegidot);
}

void wrolestp(char i){
    borrarrol('0');
    FILE* rolelegidop = fopen("rp.txt","r");
    char wrolp[300];
    int y=32,x=124+4;
    gotoxy(7,y);
    do{
        fgets(wrolp,300,rolelegidop);
        gotoxy(x,y);
        printf("%s",wrolp);
        y=y+1;
    }while(!feof(rolelegidop));
    fclose(rolelegidop);
}

void wrolestg(char i){
    borrarrol('0');
    FILE* rolelegidog = fopen("rg.txt","r");
    char wrolg[300];
    int y=32,x=124+4;
    gotoxy(7,y);
    do{
        fgets(wrolg,300,rolelegidog);
        gotoxy(x,y);
        printf("%s",wrolg);
        y=y+1;
    }while(!feof(rolelegidog));
    fclose(rolelegidog);
}

void wroleste(char i){
    borrarrol('0');
    FILE* rolelegidoe = fopen("re.txt","r");
    char wrole[300];
    int y=32,x=124+4;
    gotoxy(7,y);
    do{
        fgets(wrole,300,rolelegidoe);
        gotoxy(x,y);
        printf("%s",wrole);
        y=y+1;
    }while(!feof(rolelegidoe));
    fclose(rolelegidoe);
}

void wrolesta(char i){
    borrarrol('0');
    FILE* rolelegidoa = fopen("ra.txt","r");
    char wrola[300];
    int y=32,x=124+3;
    gotoxy(7,y);
    do{
        fgets(wrola,300,rolelegidoa);
        gotoxy(x,y);
        printf("%s",wrola);
        y=y+1;
    }while(!feof(rolelegidoa));
    fclose(rolelegidoa);
}

int main()
{

    FILE* menur5 = fopen("menuretorno.txt","w"); //dejar en blanco menu.txt
    fputc('r',menur5);
    fclose(menur5);

    int all=1;
    do{
    FILE* menurrr = fopen("menuretorno.txt","r");
    int retorno = fgetc(menurrr);
    if(retorno=='r'){


        Ymenu('0');
        quelegidomenu('0');


    }else{all=0;}
    fclose(menurrr);
    }while(all=1);

    FILE* menur4 = fopen("menuretorno.txt","w"); //dejar en blanco menu.txt
    fputc(' ',menur4);
    fclose(menur4);



    gotoxy(0,50);
    return 0;
}
